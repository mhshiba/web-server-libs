package rnojiri.base.servlet.controller.form;

import rnojiri.lib.validation.annotation.MinimumSize;
import rnojiri.lib.validation.annotation.RegexpValidation;
import rnojiri.lib.validation.annotation.RequiredParam;

/**
 * A password field.
 * 
 * @author rnojiri
 */
public interface PasswordField
{
	/**
	 * Returns the password.
	 * 
	 * @return String
	 */
	@RequiredParam
	@MinimumSize(8)
	@RegexpValidation("^[\\p{IsLatin}\\d\\`\\~\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\_\\-\\+\\=\\{\\}\\[\\]\\\\\\|\\:\\;\\\"\\'\\<\\>\\,\\.\\?\\/]+$")
	String getPassword();
}
