package rnojiri.base.servlet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import rnojiri.base.servlet.service.UrlManager;
import rnojiri.lib.shiro.spring.AbstractController;

/**
 * Parent sign in or registration controller.
 * 
 * @author rnojiri
 *
 * @param <I>
 * @param <U>
 * @param <J>
 */
@Component
public abstract class AbstractSignInOrRegistrationController<I, U, J> extends AbstractController
{
	@Autowired
	protected UrlManager urlManager;
	
	/**
	 * Uses the implementation template. 
	 * 
	 * @return String
	 */
	protected abstract String getTemplate();
	
	/**
	 * Returns the default model and view.
	 * 
	 * @return ModelAndView
	 */
	protected ModelAndView getModelAndView()
	{
		return new ModelAndView(getTemplate());
	}
	
	/**
	 * Returns the user from the database.
	 * 
	 * @param userId
	 * @return <U>
	 */
	protected abstract U getUserFromDatabase(I userId);
	
	/**
	 * Transforms to JSON.
	 * 
	 * @param user
	 * @return J
	 */
	protected abstract J toJson(U user);
}
