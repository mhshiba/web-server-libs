package rnojiri.base.servlet.controller.form;

import rnojiri.lib.validation.annotation.RegexpValidation;
import rnojiri.lib.validation.annotation.RequiredParam;

/**
 * User name fields.
 * 
 * @author rnojiri
 */
public interface UserNameFields
{
	/**
	 * Returns the fisrt name.
	 * 
	 * @return String
	 */
	@RequiredParam
	@RegexpValidation("^[\\p{IsLatin}\\'\\.]{2}[\\p{IsLatin}-\\'\\. ]*$")
	String getFirstName();
	
	/**
	 * Returns the fisrt name.
	 * 
	 * @return String
	 */
	@RequiredParam
	@RegexpValidation("^[\\p{IsLatin}\\'\\.]{2}[\\p{IsLatin}-\\'\\. ]*$")
	String getLastName();
}
