package rnojiri.base.servlet.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rnojiri.lib.collection.PreBuiltArrayList;
import rnojiri.lib.log.BeanLogger;
import rnojiri.lib.shiro.interceptor.NotAuthorizedJsonResultInterceptor;
import rnojiri.lib.shiro.interceptor.UserLoaderInterceptor;
import rnojiri.lib.shiro.service.IUserLoader;
import rnojiri.lib.spring.config.FreemarkerToolsHandler;

/**
 * Has all base servlet configuration beans.
 * 
 * @author rnojiri
 */
@Order(1)
@Configuration
@Component
public abstract class AbstractBaseServletConfig<I>
{
	private ObjectMapper objectMapper;
	
	private IUserLoader<I, String> userLoader;
	
	/**
	 * Creates a new instance of the user loader implementation.
	 * 
	 * @return IUserLoader<I, E>
	 */
	protected abstract IUserLoader<I, String> newUserLoader();
	
	/**
	 * Returns the jackson object mapper.
	 * 
	 * @return ObjectMapper
	 */
	@Bean
	public ObjectMapper objectMapper()
	{
		if(objectMapper == null)
		{
			objectMapper = new ObjectMapper();
			objectMapper.setSerializationInclusion(Include.NON_NULL);
			objectMapper.setDefaultPropertyInclusion(Include.NON_NULL);
		}
		
		return objectMapper;
	}
	
	/**
	 * User loader bean.
	 * 
	 * @return IUserLoader<I, E>
	 */
	@Bean
	public IUserLoader<I, String> userLoader()
	{
		if(userLoader == null)
		{
			userLoader = newUserLoader();
		}
		
		BeanLogger.creating(IUserLoader.class);
		
		return userLoader;
	}
	
	/**
	 * Creates the list of default interceptors.
	 * 
	 * @return List<HandlerInterceptorAdapter>
	 * @throws JsonProcessingException 
	 */
	@Bean(name = "customInterceptors")
	public List<HandlerInterceptorAdapter> customInterceptors() throws JsonProcessingException
	{
		BeanLogger.creating("customInterceptors");
		
		return new PreBuiltArrayList<HandlerInterceptorAdapter>(new UserLoaderInterceptor<>(userLoader()),
																new NotAuthorizedJsonResultInterceptor(objectMapper()),
																new FreemarkerToolsHandler());
	}
}
