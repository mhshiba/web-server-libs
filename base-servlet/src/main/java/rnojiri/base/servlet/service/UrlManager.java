package rnojiri.base.servlet.service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.session.Session;
import org.springframework.web.servlet.ModelAndView;

import rnojiri.lib.shiro.cache.SessionAttributes;
import rnojiri.lib.support.Constants;

/**
 * Manages URLs.
 * 
 * @author rnojiri
 */
public class UrlManager
{
	private static final String SPRING_REDIRECT_DIRECTIVE = "redirect:";
	
	private final String defaultRedirectUri;

	private final Set<Pattern> nonReferrableUriPatterns;

	/**
	 * @param defaultRedirectUri
	 * @param nonReferrableUriPatterns
	 */
	public UrlManager(String defaultRedirectUri, Set<Pattern> nonReferrableUriPatterns)
	{
		this.defaultRedirectUri = defaultRedirectUri;
		this.nonReferrableUriPatterns = nonReferrableUriPatterns;
	}

	/**
	 * Returns the referrable uri.
	 * 
	 * @param uri
	 * @return String
	 */
	public String getReferrableUri(String uri)
	{
		if (StringUtils.isBlank(uri))
		{
			return defaultRedirectUri;
		}

		String decodedUrl;

		try
		{
			decodedUrl = URLDecoder.decode(uri, Constants.UTF_8);
		}
		catch (UnsupportedEncodingException e)
		{
			throw new RuntimeException(e);
		}

		for (Pattern pattern : nonReferrableUriPatterns)
		{
			if (pattern.matcher(decodedUrl).find())
			{
				return defaultRedirectUri;
			}
		}

		return decodedUrl;
	}

	/**
	 * Returns the ModelAndView to redirect.
	 * 
	 * @param redirectUrl
	 * @param session
	 * @return ModelAndView
	 * @throws UnsupportedEncodingException
	 */
	public ModelAndView getRedirectModelAndView(String redirectUrl, Session session)
	{
		return buildRedirectModelAndView(getRedirectUrl(redirectUrl, session));
	}
	
	/**
	 * Returns the redirect url.
	 * 
	 * @param redirectUrl
	 * @param session
	 * @return String
	 */
	public String getRedirectUrl(String redirectUrl, Session session)
	{
		if (StringUtils.isNotBlank(redirectUrl))
		{
			return redirectUrl;
		}
		
		return getReferrableUri((String) session.getAttribute(SessionAttributes.REFERRER));
	}

	/**
	 * Returns the ModelAndView to redirect.
	 * 
	 * @param url
	 * @return ModelAndView
	 */
	public ModelAndView buildRedirectModelAndView(String url)
	{
		return new ModelAndView(SPRING_REDIRECT_DIRECTIVE + url);
	}
}
