package rnojiri.lib.shiro.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import rnojiri.lib.interfaces.IMultiId;
import rnojiri.lib.shiro.annotation.AddUserToView;
import rnojiri.lib.shiro.cache.SessionAttributes;
import rnojiri.lib.shiro.service.IUserLoader;

/**
 * Loads the user from database on the session if needed.
 * 
 * @author rnojiri
 */
public class UserLoaderInterceptor<I, E> extends HandlerInterceptorAdapter
{
	private IUserLoader<I, E> userLoader;
	
	/**
	 * @param userLoader
	 */
	public UserLoaderInterceptor(IUserLoader<I, E> userLoader)
	{
		this.userLoader = userLoader;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
	{
		Subject subject = SecurityUtils.getSubject();
		
		if(subject.isAuthenticated())
		{
			Session session = subject.getSession();
			
			IMultiId<I, E> user = (IMultiId<I, E>)session.getAttribute(SessionAttributes.USER); 
			
			if(user == null)
			{
				user = userLoader.loadUserByExternalId((E)subject.getPrincipal());
				
				session.setAttribute(SessionAttributes.USER, user);
			}
		}
		
		return true;
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
	{
		if(handler instanceof HandlerMethod)
		{
			HandlerMethod handlerMethod = (HandlerMethod)handler;
			
			AddUserToView addUserToView = handlerMethod.getMethodAnnotation(AddUserToView.class);
			
			if(addUserToView != null && modelAndView != null)
			{
				modelAndView.addObject(SessionAttributes.USER, SecurityUtils.getSubject().getSession().getAttribute(SessionAttributes.USER));
			}
		}
	}
}
