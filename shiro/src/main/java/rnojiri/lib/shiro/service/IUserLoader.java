package rnojiri.lib.shiro.service;

import rnojiri.lib.interfaces.IMultiId;

/**
 * Interface for a service to load a user.
 * 
 * @author rnojiri
 *
 * @param <I> (the internal ID from the user)
 * @param <E> (the external ID from the user)
 */
public interface IUserLoader<I, E>
{
	/**
	 * Loads an user by his internal ID.
	 * 
	 * @param id
	 * @return IUser<I, E>
	 */
	IMultiId<I, E> loadUserById(I id);
	
	/**
	 * Loads an user using his external ID.
	 * 
	 * @param externalId
	 * @return IUser<I, E>
	 */
	IMultiId<I, E> loadUserByExternalId(E externalId);
}
