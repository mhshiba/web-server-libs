package rnojiri.lib.shiro.filter;

import java.util.ArrayList;

/**
 * A typed list.
 * 
 * @author rnojiri
 */
public class FilterConfigList extends ArrayList<FilterConfig>
{
	private static final long serialVersionUID = -9037805007087895486L;
}
