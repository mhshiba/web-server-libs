package rnojiri.lib.shiro.jetty;

import java.io.IOException;
import java.util.EnumSet;
import java.util.List;

import javax.servlet.DispatcherType;

import org.springframework.web.filter.DelegatingFilterProxy;

import rnojiri.jetty.FilterItem;
import rnojiri.lib.spring.jetty.SpringEmbeddedJetty;
import rnojiri.lib.util.PropertiesReader;

/**
 * Integrates Shiro to the Spring servlet.
 * 
 * @author rnojiri
 */
public abstract class ShiroEmbeddedJetty extends SpringEmbeddedJetty
{
	public static final String SHIRO_FILTER_NAME = "shiroFilter";
	
	/**
	 * @param propertiesFile
	 * @throws IOException
	 */
	public ShiroEmbeddedJetty(PropertiesReader properties) throws IOException
	{
		super(properties);
	}

	@Override
	protected List<FilterItem> getFilters()
	{
		DelegatingFilterProxy shiroFilter = new DelegatingFilterProxy();
		shiroFilter.setTargetFilterLifecycle(true);
		shiroFilter.setTargetBeanName(SHIRO_FILTER_NAME);
		
		List<FilterItem> filters = super.getFilters();
		filters.add(new FilterItem(shiroFilter, "/*", EnumSet.of(DispatcherType.REQUEST, DispatcherType.ERROR, DispatcherType.FORWARD, DispatcherType.INCLUDE)));
		
		return filters;
	}
}
