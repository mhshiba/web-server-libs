package rnojiri.lib.shiro.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;

import rnojiri.lib.cache.CacheInstance;

/**
 * Memcached version for the shiro cache manager.
 * 
 * @author rnojiri
 */
public class CacheInstanceCacheManager implements CacheManager
{
	private CacheInstance cacheInstance;
	
	private int ttl;
	
	/**
	 * @param cacheInstance
	 * @param ttl
	 */
	public CacheInstanceCacheManager(CacheInstance cacheInstance, int ttl)
	{
		this.cacheInstance = cacheInstance;
		this.ttl = ttl;
	}
	
	@Override
	public <K, V> Cache<K, V> getCache(String name) throws CacheException
	{
		return new CacheInstanceCache<K, V>(cacheInstance, ttl);
	}
}
