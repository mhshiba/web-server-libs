package rnojiri.lib.http;

/**
 * The HttpRequestUtil response structure.
 * 
 * @author rnojiri
 */
public class HttpRequestUtilResponse
{
	public final int status;
	
	public final String body;

	/**
	 * @param status
	 * @param body
	 */
	public HttpRequestUtilResponse(int status, String body)
	{
		super();
		this.status = status;
		this.body = body;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "HttpRequestUtilResponse [status=" + status + ", body=" + body + "]";
	}
}
