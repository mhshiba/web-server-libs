package rnojiri.lib.http;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;

/**
 * Utility functions for cookies.
 * 
 * @author rnojiri
 */
public class CookieUtil
{
	public static final String DEFAULT_COOKIE_PATH = "/";
	
	private CookieUtil()
	{
		;
	}
	
	/**
	 * Adds a cookie to the response.
	 * 
	 * @param cookieName
	 * @param value
	 * @param cookieSessionTTLInMillis
	 * @param domain
	 * @param response
	 * @param log
	 * @return boolean
	 */
	public static boolean addCookie(String cookieName, 
									String value, 
									int cookieSessionTTLInMillis, 
									String domain, 
									HttpServletResponse response, 
									Logger log)
	{
		try
		{
			Cookie cookie = new Cookie(cookieName, value);
			cookie.setMaxAge(cookieSessionTTLInMillis);
			cookie.setPath(DEFAULT_COOKIE_PATH);
			
			response.addCookie(cookie);
			
			if(log.isDebugEnabled())
			{
				log.debug("Cookie \"" + cookieName + "\" was added with value \"" + value + "\".");
			}
			
			return true;
		}
		catch(Exception e)
		{
			log.error("Error adding cookie \"" + cookieName + "\" with value \"" + value + "\".", e);
			
			return false;
		}
	}
	
	/**
	 * Returns the value from a cookie.
	 * 
	 * @param cookieName
	 * @param request
	 * @param logger
	 * @return String
	 */
	public static String getCookieValue(final String cookieName, HttpServletRequest request, Logger logger)
	{
		Cookie cookies[] = request.getCookies();
		
		if(cookies == null || cookies.length == 0)
		{
			if(logger.isDebugEnabled())
			{
				logger.debug("No cookie was found in the request.");
			}
			
			return null;
		}
		
		Cookie selectedCookie = null;
		
	    for(Cookie cookie : cookies)
	    {
	    	if(cookieName.equals(cookie.getName()))
	    	{
	    		selectedCookie = cookie;
	    		
	    		break;
	    	}
	    }
	    
	    if(selectedCookie == null)
	    {
	    	if(logger.isDebugEnabled())
			{
				logger.debug("No cookie with name \"" + cookieName + "\" was found in the request.");
			}
	    	
	    	return null;
	    }
	    
	    return selectedCookie.getValue();
	}
}
