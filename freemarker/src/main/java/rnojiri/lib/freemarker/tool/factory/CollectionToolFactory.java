package rnojiri.lib.freemarker.tool.factory;

import javax.servlet.http.HttpServletRequest;

import rnojiri.lib.freemarker.tool.CollectionTool;

/**
 * Collection tool factory.
 * 
 * @author rnojiri
 */
public class CollectionToolFactory implements ToolFactory<CollectionTool>
{
	private CollectionTool collectionTool;
	
	public CollectionToolFactory()
	{
		collectionTool = new CollectionTool();
	}
	
	@Override
	public CollectionTool getInstance(HttpServletRequest httpServletRequest)
	{
		return collectionTool;
	}
}
