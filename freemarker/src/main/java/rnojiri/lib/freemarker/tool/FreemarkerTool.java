package rnojiri.lib.freemarker.tool;


/**
 * A tool for Freemarker template engine.
 * 
 * @author rnojiri
 */
public interface FreemarkerTool
{
	/**
	 * Returns the tool's name.
	 * 
	 * @return String
	 */
	String getName();
}
