package rnojiri.lib.freemarker.tool;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * A tool to parse raw request variables.
 * 
 * @author rnojiri
 */
public class RequestTool implements FreemarkerTool
{
	private static final String NAME = "requestTool";
	
	private static final ParseValue<Integer> INTEGER_PARSER = new ParseValue<Integer>()
	{
		@Override
		protected Integer parseImplementation(String[] value)
		{
			return Integer.parseInt(value[0]) ;
		}
	}; 
	
	private static final ParseValue<Integer[]> INTEGER_ARRAY_PARSER = new ParseValue<Integer[]>()
	{
		@Override
		protected Integer[] parseImplementation(String[] value)
		{
			Integer array[] = new Integer[value.length];
			
			for(int i=0; i<value.length; i++)
			{
				array[i] = Integer.parseInt(value[i]);
			}
			
			return array;
		}
	}; 
	
	private static final ParseValue<Float> FLOAT_PARSER = new ParseValue<Float>()
	{
		@Override
		protected Float parseImplementation(String[] value)
		{
			return Float.parseFloat(value[0]) ;
		}
	}; 
	
	private static final ParseValue<Float[]> FLOAT_ARRAY_PARSER = new ParseValue<Float[]>()
	{
		@Override
		protected Float[] parseImplementation(String[] value)
		{
			Float array[] = new Float[value.length];
			
			for(int i=0; i<value.length; i++)
			{
				array[i] = Float.parseFloat(value[i]);
			}
			
			return array;
		}
	}; 
	
	private static final ParseValue<String> STRING_PARSER = new ParseValue<String>()
	{
		@Override
		protected String parseImplementation(String[] value)
		{
			return value[0];
		}
	}; 
	
	private static final ParseValue<String[]> STRING_ARRAY_PARSER = new ParseValue<String[]>()
	{
		@Override
		protected String[] parseImplementation(String[] value)
		{
			return value;
		}
	}; 
	
	private Map<String, String[]> parameterMap;
	
	/**
	 * @param request
	 */
	@SuppressWarnings("unchecked")
	public RequestTool(HttpServletRequest request)
	{
		parameterMap = request.getParameterMap();
	}
	
	/**
	 * Returns an Integer.
	 * 
	 * @param parameterName
	 * @return Integer
	 */
	public Integer getInteger(final String parameterName)
	{
		return INTEGER_PARSER.parse(parameterName, parameterMap);
	}
	
	/**
	 * Returns an Integer array.
	 * 
	 * @param parameterName
	 * @return Integer[]
	 */
	public Integer[] getIntegerArray(final String parameterName)
	{
		return INTEGER_ARRAY_PARSER.parse(parameterName, parameterMap);
	}
	
	/**
	 * Returns a Float.
	 * 
	 * @param parameterName
	 * @return Float
	 */
	public Float getFloat(final String parameterName)
	{
		return FLOAT_PARSER.parse(parameterName, parameterMap);
	}
	
	/**
	 * Returns an Float array.
	 * 
	 * @param parameterName
	 * @return Float[]
	 */
	public Float[] getFloatArray(final String parameterName)
	{
		return FLOAT_ARRAY_PARSER.parse(parameterName, parameterMap);
	}
	
	/**
	 * Returns a String.
	 * 
	 * @param parameterName
	 * @return String
	 */
	public String getString(final String parameterName)
	{
		return STRING_PARSER.parse(parameterName, parameterMap);
	}
	
	/**
	 * Returns an String array.
	 * 
	 * @param parameterName
	 * @return String[]
	 */
	public String[] getStringArray(final String parameterName)
	{
		return STRING_ARRAY_PARSER.parse(parameterName, parameterMap);
	}
	
	/**
	 * Abstracts the parsing.
	 * 
	 * @author rnojiri
	 *
	 * @param <T>
	 */
	private abstract static class ParseValue<T>
	{
		/**
		 * Parses the string array.
		 * 
		 * @param value
		 * @return Object
		 */
		public T parse(String parameterName, Map<String, String[]> parameterMap)
		{
			String[] value = parameterMap.get(parameterName);
			
			if(value != null && value.length > 0)
			{
				return parseImplementation(value);
			}
			
			return null;
		}
		
		/**
		 * Parses the string array implementation.
		 * 
		 * @param value
		 * @return Object
		 */
		protected abstract T parseImplementation(String[] value);
	}

	@Override
	public String getName()
	{
		return NAME;
	}
}
