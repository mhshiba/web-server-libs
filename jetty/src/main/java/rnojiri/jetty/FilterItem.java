package rnojiri.jetty;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;

import org.eclipse.jetty.servlet.FilterHolder;

/**
 * A filter item to be installed in the servlet.
 * 
 * @author rnojiri
 */
public class FilterItem
{
	final private FilterHolder filterHolder;
	
	final private String context;
	
	final private EnumSet<DispatcherType> dispatcherType;

	/**
	 * @param filter
	 * @param context
	 * @param dispatcherType
	 */
	public FilterItem(Filter filter, String context, EnumSet<DispatcherType> dispatcherType)
	{
		super();
		filterHolder = new FilterHolder(filter);
		this.context = context;
		this.dispatcherType = dispatcherType;
	}
	
	/**
	 * Returns the filter holder.
	 * 
	 * @return FilterHolder
	 */
	public FilterHolder getFilterHolder()
	{
		return filterHolder;
	}

	/**
	 * @return the context
	 */
	public String getContext()
	{
		return context;
	}

	/**
	 * @return the dispatcherType
	 */
	public EnumSet<DispatcherType> getDispatcherType()
	{
		return dispatcherType;
	}
}
