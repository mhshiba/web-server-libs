package rnojiri.jetty.session;

import rnojiri.lib.cache.CacheInstance;

/**
 * Custom cache instance interface.
 * 
 * @author rnojiri
 */
public interface CustomSessionDataStore
{
	/**
	 * Sets the cache instance.
	 * 
	 * @param cacheInstance
	 */
	void setCacheInstance(CacheInstance cacheInstance);
}
