package rnojiri.jetty.session;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.spy.memcached.BinaryConnectionFactory;
import net.spy.memcached.CASValue;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.internal.OperationFuture;
import rnojiri.lib.cache.CacheInstance;

/**
 * A memcached cache instance.
 * 
 * @author rnojiri
 */
public class MemcachedCacheInstance implements CacheInstance
{
	public static final String STATS_TOTAL_ITEMS = "total_items";
	
	private MemcachedClient memcachedClient; 
	
	/**
	 * @param instances in format <host, port>
	 * @throws IOException
	 */
	public MemcachedCacheInstance(Map<String, Integer> instances) throws IOException
	{
		if(instances == null || instances.isEmpty())
		{
			throw new RuntimeException("No memcached instances found.");
		}
		
		List<InetSocketAddress> socketAddresses = new ArrayList<>();
		
		for(Entry<String, Integer> instance : instances.entrySet())
		{
			socketAddresses.add(new InetSocketAddress(instance.getKey(), instance.getValue()));
		}
		
		memcachedClient = new MemcachedClient(new BinaryConnectionFactory(), socketAddresses);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T get(String key) throws Exception
	{
		return (T)memcachedClient.get(key);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T get(String key, int ttl) throws Exception
	{
		CASValue<T> casValue = (CASValue<T>)memcachedClient.getAndTouch(key, ttl);
		
		if(casValue == null)
		{
			return null;
		}
		
		return casValue.getValue();
	}

	@Override
	public void set(String key, int ttl, Object object) throws Exception
	{
		memcachedClient.set(key, ttl, object);
	}

	@Override
	public boolean remove(String key) throws Exception
	{
		OperationFuture<Boolean> result = memcachedClient.delete(key);
		
		if(result == null)
		{
			return false;
		}
		
		return result.get();
	}

	@Override
	public void shutdown()
	{
		memcachedClient.shutdown();
	}

	@Override
	public void clear() throws Exception
	{
		memcachedClient.flush().isDone();
	}

	@Override
	public int size()
	{
		int size = 0;
		
		for(Map<String, String> map : memcachedClient.getStats().values())
		{
			size += Integer.parseInt(map.get(STATS_TOTAL_ITEMS));
		}
		
		return size;
	}
}
