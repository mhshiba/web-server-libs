package rnojiri.lib.spring.handler;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.lang.Nullable;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.util.WebUtils;

import rnojiri.lib.support.JsonResult;
import rnojiri.lib.util.ReflectionUtil;

/**
 * Handles all non caught exceptions raised by controllers.
 * 
 * @author rnojiri
 */
@ControllerAdvice
public class ExceptionResponseHandler
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionResponseHandler.class);

	/**
	 * Provides handling for standard Spring MVC exceptions.
	 * 
	 * @param ex
	 * @param request
	 * @param handlerMethod
	 * @return ResponseEntity<Object>
	 * @throws Exception
	 */
	@ExceptionHandler({ HttpRequestMethodNotSupportedException.class, HttpMediaTypeNotSupportedException.class, HttpMediaTypeNotAcceptableException.class,
			MissingPathVariableException.class, MissingServletRequestParameterException.class, ServletRequestBindingException.class,
			ConversionNotSupportedException.class, TypeMismatchException.class, HttpMessageNotReadableException.class, HttpMessageNotWritableException.class,
			MethodArgumentNotValidException.class, MissingServletRequestPartException.class, BindException.class, NoHandlerFoundException.class,
			AsyncRequestTimeoutException.class })
	@Nullable
	public final ResponseEntity<Object> handleException(Exception ex, WebRequest request, HandlerMethod handlerMethod) throws Exception
	{
		HttpHeaders headers = new HttpHeaders();

		if(ex instanceof HttpRequestMethodNotSupportedException)
		{
			HttpStatus status = HttpStatus.METHOD_NOT_ALLOWED;
			return handleExceptionInternal((HttpRequestMethodNotSupportedException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof HttpMediaTypeNotSupportedException)
		{
			HttpStatus status = HttpStatus.UNSUPPORTED_MEDIA_TYPE;
			return handleExceptionInternal((HttpMediaTypeNotSupportedException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof HttpMediaTypeNotAcceptableException)
		{
			HttpStatus status = HttpStatus.NOT_ACCEPTABLE;
			return handleExceptionInternal((HttpMediaTypeNotAcceptableException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof MissingPathVariableException)
		{
			HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
			return handleExceptionInternal((MissingPathVariableException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof MissingServletRequestParameterException)
		{
			HttpStatus status = HttpStatus.BAD_REQUEST;
			return handleExceptionInternal((MissingServletRequestParameterException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof ServletRequestBindingException)
		{
			HttpStatus status = HttpStatus.BAD_REQUEST;
			return handleExceptionInternal((ServletRequestBindingException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof ConversionNotSupportedException)
		{
			HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
			return handleExceptionInternal((ConversionNotSupportedException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof TypeMismatchException)
		{
			HttpStatus status = HttpStatus.BAD_REQUEST;
			return handleExceptionInternal((TypeMismatchException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof HttpMessageNotReadableException)
		{
			HttpStatus status = HttpStatus.BAD_REQUEST;
			return handleExceptionInternal((HttpMessageNotReadableException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof HttpMessageNotWritableException)
		{
			HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
			return handleExceptionInternal((HttpMessageNotWritableException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof MethodArgumentNotValidException)
		{
			HttpStatus status = HttpStatus.BAD_REQUEST;
			return handleExceptionInternal((MethodArgumentNotValidException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof MissingServletRequestPartException)
		{
			HttpStatus status = HttpStatus.BAD_REQUEST;
			return handleExceptionInternal((MissingServletRequestPartException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof BindException)
		{
			HttpStatus status = HttpStatus.BAD_REQUEST;
			return handleExceptionInternal((BindException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof NoHandlerFoundException)
		{
			HttpStatus status = HttpStatus.NOT_FOUND;
			return handleExceptionInternal((NoHandlerFoundException) ex, headers, status, request, handlerMethod);
		}
		else if(ex instanceof AsyncRequestTimeoutException)
		{
			HttpStatus status = HttpStatus.SERVICE_UNAVAILABLE;
			return handleAsyncRequestTimeoutException((AsyncRequestTimeoutException) ex, headers, status, request, handlerMethod);
		}
		else
		{
			// Unknown exception, typically a wrapper with a common MVC exception as cause
			// (since @ExceptionHandler type declarations also match first-level causes):
			// We only deal with top-level MVC exceptions here, so let's rethrow the given
			// exception for further processing through the HandlerExceptionResolver chain.
			throw ex;
		}
	}

	/**
	 * Customize the response for NoHandlerFoundException.
	 * 
	 * @param ex
	 * @param headers
	 * @param status
	 * @param webRequest
	 * @param handlerMethod
	 * @return ResponseEntity<Object>
	 */
	@Nullable
	protected ResponseEntity<Object> handleAsyncRequestTimeoutException(AsyncRequestTimeoutException ex, HttpHeaders headers, HttpStatus status, WebRequest webRequest, HandlerMethod handlerMethod)
	{
		if(webRequest instanceof ServletWebRequest)
		{
			ServletWebRequest servletWebRequest = (ServletWebRequest) webRequest;
			HttpServletRequest request = servletWebRequest.getRequest();
			HttpServletResponse response = servletWebRequest.getResponse();
			
			if(response != null && response.isCommitted())
			{
				if(LOGGER.isDebugEnabled())
				{
					LOGGER.debug("Async timeout for " + request.getMethod() + " [" + request.getRequestURI() + "]");
				}
				
				return null;
			}
		}

		return handleExceptionInternal(ex, headers, status, webRequest, handlerMethod);
	}

	/**
	 * A single place to customize the response body of all Exception types.
	 * 
	 * @param ex
	 * @param headers
	 * @param status
	 * @param request
	 * @param handlerMethod
	 * @return ResponseEntity<Object>
	 */
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, HttpHeaders headers, HttpStatus status, WebRequest request, HandlerMethod handlerMethod)
	{
		if(HttpStatus.INTERNAL_SERVER_ERROR.equals(status))
		{
			request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
		}
		
		final MediaType mediaType = headers.getContentType();

		if(mediaType != null && mediaType.isCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
		{
			return new ResponseEntity<Object>(new JsonResult<Integer, String>(status.value()), headers, status);
		}
		else if(handlerMethod != null)
		{
			Class<?> clazz = handlerMethod.getBeanType();
			Method method = handlerMethod.getMethod();
			
			if(ReflectionUtil.isAnnotationPresent(clazz, ResponseBody.class, RestController.class) ||
			   ReflectionUtil.isAnnotationPresent(method, ResponseBody.class))
			{
				return new ResponseEntity<Object>(new JsonResult<Integer, String>(status.value()), headers, status);
			}
		}

		return new ResponseEntity<>(headers, status);
	}

}
