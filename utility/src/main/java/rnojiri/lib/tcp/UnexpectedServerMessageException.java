package rnojiri.lib.tcp;

/**
 * Raised when a unexpected message from server arrives.
 * 
 * @author rnojiri
 */
public class UnexpectedServerMessageException extends Exception
{
	private static final long serialVersionUID = -8654536868450956173L;

	/**
	 * @param message
	 */
	public UnexpectedServerMessageException(String message)
	{
		super("Received an unexpected message from server: \"" + message + "\".");
	}
}
