package rnojiri.lib.tcp;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Map;
import java.util.Map.Entry;

/**
 * A generic protocol.
 * 
 * @author rnojiri
 */
public abstract class Protocol
{	
	private Map<String, MessageFormat> commandMap;
	
	public Protocol()
	{
		this.commandMap = buildCommandMap();
	}
	
	/**
	 * Builds the command map called in the constructor.
	 * 
	 * @return Map<Byte, MessageFormat>
	 */
	protected abstract Map<String, MessageFormat> buildCommandMap();
	
	/**
	 * Converts a command in string format.
	 * 
	 * @param command
	 * @param args
	 * @return String
	 * @throws UnknownProtocolCommandException
	 */
	public String convert(String command, Object ... args) throws UnknownProtocolCommandException
	{
		MessageFormat commandFormat = commandMap.get(command);
		
		if(commandFormat == null)
		{
			throw new UnknownProtocolCommandException(command);
		}
		
		return commandFormat.format(args);
	}
	
	/**
	 * Returns the command in object format.
	 * 
	 * @param commandLine
	 * @return Command
	 * @throws UnknownProtocolCommandException
	 */
	public Command translate(String commandLine) throws UnknownProtocolCommandException
	{
		for(Entry<String, MessageFormat> entry : commandMap.entrySet())
		{
			try
			{
				Object[] parameters = entry.getValue().parse(commandLine);
				
				String commandName = entry.getKey();
				
				return new Command(commandName, parameters);
			}
			catch(ParseException e)
			{
				continue;
			}
		}
		
		throw new UnknownProtocolCommandException(commandLine);
	}
	
	/**
	 * Returns the protocol OK response.
	 * 
	 * @return String
	 */
	public abstract String getOkResponse();
	
	/**
	 * Returns the protocol WAITING response.
	 * 
	 * @return String
	 */
	public abstract String getWaitingResponse();
	
	/**
	 * Returns the protocol UNKNOWN response.
	 * 
	 * @return String
	 */
	public abstract String getUnknownResponse();
	
	/**
	 * Returns the protocol ERROR response.
	 * 
	 * @return String
	 */
	public abstract String getErrorResponse();
	
	/**
	 * Returns the protocol END response.
	 * 
	 * @return String
	 */
	public abstract String getEndResponse();
	
	/**
	 * Returns the protocol END command.
	 * 
	 * @return String
	 */
	public abstract String getEndCommand();
}
