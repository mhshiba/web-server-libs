package rnojiri.lib.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Send all protocol commands to the listener.
 * 
 * @author rnojiri
 */
public class SocketWriter
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SocketWriter.class);
	
	private final String hostName;
	
	private final int port;
	
	private Socket socket;
	
	private PrintWriter out;
	
	private BufferedReader in;
	
	private final Protocol protocol;
	
	private final String okResponse;
	
	private final String waitingResponse;
	
	/**
	 * @param hostname
	 * @param port
	 * @param protocol
	 */
	public SocketWriter(String hostName, int port, Protocol protocol)
	{
		this.hostName = hostName;
		this.port = port;
		this.protocol = protocol;
		
		okResponse = protocol.getOkResponse();
		waitingResponse = protocol.getWaitingResponse();
	}
	
	/**
	 * Sends the shuffle command.
	 * 
	 * @param idUser
	 * @param idPlan
	 * @throws IOException 
	 * @throws UnknownHostException 
	 * @throws UnexpectedServerMessageException 
	 * @throws UnknownProtocolCommandException 
	 */
	public void sendCommand(String command, Object ... args) throws UnknownHostException, IOException, UnexpectedServerMessageException, UnknownProtocolCommandException
	{
		refreshSocket();
		
		String commandString = protocol.convert(command, args);
		
		if(LOGGER.isDebugEnabled())
		{
			LOGGER.debug("Sending command \"" + commandString + "\" to server.");
		}
		
		out.println(commandString);
		
		String fromServer = in.readLine();
		
		if(fromServer.equals(okResponse))
		{
			LOGGER.info("Server answered OK for command: \"" + commandString + "\"");
		}
		else
		{
			LOGGER.error("Server answered an error: \"" + fromServer + "\"");
		}
	}
	
	/**
	 * Returns the socket.
	 * 
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws UnexpectedServerMessageException 
	 */
	private void refreshSocket() throws UnknownHostException, IOException, UnexpectedServerMessageException
	{
		if(socket == null || in == null || out == null || 
		   socket.isClosed() || !socket.isConnected() || !socket.isBound() ||
		   socket.isInputShutdown() || socket.isOutputShutdown() || 
		   !in.ready() || out.checkError())
		{
			closeSocket();
			
			connectSocket();
			
			String fromServer = in.readLine();
			
			if(fromServer == null)
			{
				throw new IOException("Message received from server is null");
			}
			
			if(fromServer.equals(waitingResponse))
			{
				if(LOGGER.isDebugEnabled())
				{
					LOGGER.debug("Received " + waitingResponse + " from server.");
				}
			}
			else
			{
				LOGGER.error("Received an unknown message \"" + fromServer + "\" from server.");
				
				throw new UnexpectedServerMessageException(fromServer);
			}
		}
	}

	/**
	 * Connects to the server.
	 * 
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	private void connectSocket() throws UnknownHostException, IOException
	{
		socket = new Socket(hostName, port);
		out = new PrintWriter(socket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        
        LOGGER.info("Connected to " + hostName + ":" + port);
	}

	/**
	 * Closes the socket.
	 */
	private void closeSocket()
	{
		if(socket != null)
		{
			try
			{
				out.close();
				out = null;
				
				in.close();
				in = null;
				
				socket.shutdownInput();
				socket.shutdownOutput();
				socket.close();
				socket = null;
				
				LOGGER.info("Connection with " + hostName + ":" + port + " was closed.");
			}
			catch(IOException e)
			{
				LOGGER.error("Error closing client socket.", e);
			}
		}
	}
	
	/**
	 * Shuts down this client.
	 */
	public void shutdown()
	{
		closeSocket();
	}
}
