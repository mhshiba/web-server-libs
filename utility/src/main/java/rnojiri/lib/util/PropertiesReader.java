package rnojiri.lib.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

/**
 * Reads a property file in the classpath.
 * 
 * @author rnojiri
 */
public class PropertiesReader extends Properties
{
	private static final long serialVersionUID = -4270949007992839034L;
	
	private static final String LIST_SEPARATOR = ",";

	/**
	 * Reads the property file.
	 * 
	 * @param file
	 * @throws IOException
	 */
	public PropertiesReader(String file) throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(file);
		
		if(inputStream == null)
		{
			inputStream = new FileInputStream(file);
		}
		
		try
		{
			load(inputStream);
		}
		catch(IOException e)
		{
			inputStream.close();
			
			throw e;
		}
	}
	
	/**
	 * Returns an array of properties.
	 * 
	 * @param key
	 * @return String[]
	 */
	public String[] getArray(String key)
	{
		String parameters = getProperty(key);
		
		String parameterArray[] = null;
		
		if(StringUtils.isNotBlank(parameters))
		{
			parameterArray = parameters.split(LIST_SEPARATOR);
		}
		
		return parameterArray;
	}
	
	/**
	 * Returns the casted value from the property.
	 * 
	 * @param clazz
	 * @param key
	 * @return T
	 * @throws UnsupportedTypeException
	 */
	@SuppressWarnings("unchecked")
	public <T> T getCastedProperty(Class<T> clazz, String key) throws UnsupportedTypeException
	{
		String parameter = getProperty(key);
		
		if(parameter == null) return null;
		
		return (T)ReflectionUtil.parseValue(clazz, parameter);
	}
}
