package rnojiri.lib.collection;

import java.util.concurrent.ConcurrentSkipListMap;

import rnojiri.lib.support.KeyValue;
import rnojiri.lib.support.RangedValue;

/**
 * A pre-built navigable map.
 * 
 * @author rnojiri
 *
 * @param <K>
 * @param <RangedValue>
 */
public class PreBuiltNavigableMap<K extends Number, V> extends ConcurrentSkipListMap<K, V>
{
	private static final long serialVersionUID = 510422293575831429L;

	/**
	 * @param keyValues
	 */
	public PreBuiltNavigableMap(@SuppressWarnings("unchecked") KeyValue<RangedValue<K>, V> ... keyValues)
	{
		super();
		
		if(keyValues != null && keyValues.length > 0)
		{
			for(KeyValue<RangedValue<K>, V> item : keyValues)
			{
				RangedValue<K> rangedValue =  item.getKey();
				V value = item.getValue();
				
				this.put(rangedValue.getStart(), value);
				this.put(rangedValue.getEnd(), value);
			}
		}
	}
	
	/**
	 * Finds the value. 
	 * 
	 * @param key
	 * @return V
	 */
	public V find(K key)
	{
		K ceilingKey = this.ceilingKey(key);
		
		if(ceilingKey == null) return null;
		
		return this.get(ceilingKey);
	}
}
