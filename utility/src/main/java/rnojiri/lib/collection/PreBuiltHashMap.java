package rnojiri.lib.collection;

import java.util.HashMap;

import rnojiri.lib.support.KeyValue;

/**
 * A pre-built hash map.
 * 
 * @author root
 *
 * @param <K>
 * @param <V>
 */
public class PreBuiltHashMap<K, V> extends HashMap<K, V>
{
	private static final long serialVersionUID = -7877246355487643700L;
	
	/**
	 * Adds all keys and values to the map.
	 * 
	 * @param keys
	 * @param values
	 */
	public PreBuiltHashMap(KeyValue<K, V> items[])
	{
		if(items != null && items.length > 0)
		{
			for(int i=0; i<items.length; i++)
			{
				put(items[i].getKey(), items[i].getValue());
			}
		}
	}
}
