package rnojiri.lib.validation;

/**
 * Validation errors.
 * 
 * @author rnojiri
 */
public enum ValidationError
{
	EMPTY_REQUIRED_FIELD,
	
	SET_METHOD_ERROR,
	
	PARAMETER_TYPE_IS_NOT_SUPPORTED,
	
	REGULAR_EXPRESSION_MISMATCH,
	
	BELOW_MINIMUM_SIZE,
	
	ABOVE_MAXIMUM_SIZE,
	
	INVALID_PARAMETER_OPTION,
	
	POST_VERIFICATION;
}
