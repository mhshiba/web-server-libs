package rnojiri.lib.validation;

/**
 * A custom business validation interface.
 * 
 * @author rnojiri
 */
public abstract class PostValidation<F extends Validable> 
{
	private final String errorCode;
	
	public PostValidation(String errorCode)
	{
		this.errorCode = errorCode;
	}
	
	/**
	 * Validates the class and returns the result.
	 * 
	 * @param form
	 * @return boolean
	 */
	public boolean isValid(final Object form)
	{
		return validateForm(cast(form));
	}
	
	/**
	 * Validates the form and returns the result.
	 * 
	 * @param form
	 * @return boolean
	 */
	protected abstract boolean validateForm(final F form);
	
	/**
	 * Casts the form.
	 * 
	 * @param form
	 * @return F
	 */
	protected abstract F cast(Object form);
	
	/**
	 * Returns the error code.
	 * 
	 * @return String
	 */
	public String getErrorCode()
	{
		return errorCode;
	}
}
