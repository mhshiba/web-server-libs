package rnojiri.lib.validation.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * A required field.
 * 
 * @author rnojiri
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RequiredParamIf
{
	/**
	 * The selected parameter.
	 * 
	 * @return String
	 */
	String parameter();
	
	/**
	 * The selected value.
	 * 
	 * @return String
	 */
	String value();
}
