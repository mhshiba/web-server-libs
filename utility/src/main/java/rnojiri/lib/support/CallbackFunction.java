package rnojiri.lib.support;

/**
 * A generic callback function.
 * 
 * @author rnojiri
 *
 * @param <R>
 */
public interface CallbackFunction<R, E extends Exception>
{
	/**
	 * A function to be executed;
	 * 
	 * @return <R>
	 * @throws <E>
	 */
	R execute() throws E;
}
