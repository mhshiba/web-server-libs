package rnojiri.lib.support;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple xml properties loader.
 * 
 * @author rnojiri
 */
public class XmlPropertiesLoader
{
	private static Logger log = LoggerFactory.getLogger(XmlPropertiesLoader.class);

	private Properties properties;

	/**
	 * Loads the properties from xml file.
	 * 
	 * @param propertiesXml
	 * @throws InvalidPropertiesFormatException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public XmlPropertiesLoader(final String propertiesXml) throws InvalidPropertiesFormatException, FileNotFoundException, IOException
	{
		String path = null;

		// if is not in classpath
		if (propertiesXml.indexOf(System.getProperty("file.separator")) > 0)
		{
			path = propertiesXml;
		}
		else
		{
			path = XmlPropertiesLoader.class.getClassLoader().getResource(propertiesXml).getPath();
		}

		properties = new Properties();

		log.debug("Loading properties XML from: " + path);

		FileInputStream fileInputStream = new FileInputStream(path);
		BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

		properties.loadFromXML(bufferedInputStream);

		bufferedInputStream.close();
		fileInputStream.close();

		log.info("Properties XML file loaded from: \"" + propertiesXml + "\".");
	}

	/**
	 * Returns a String.
	 * 
	 * @param propertyName
	 * @return String
	 */
	public String getString(final String propertyName)
	{
		String value = properties.getProperty(propertyName);

		if (log.isDebugEnabled())
		{
			log.debug("Property \"" + propertyName + "\" = \"" + value + "\"");
		}

		return value;
	}

	/**
	 * Returns a Long.
	 * 
	 * @param propertyName
	 * @return Long
	 */
	public Long getLong(final String propertyName)
	{
		String value = getString(propertyName);

		if (value == null)
		{
			return null;
		}

		return Long.parseLong(value);
	}

	/**
	 * Returns a Integer.
	 * 
	 * @param propertyName
	 * @return Integer
	 */
	public Integer getInteger(final String propertyName)
	{
		String value = getString(propertyName);

		if (value == null)
		{
			return null;
		}

		return Integer.parseInt(value);
	}

	/**
	 * Returns a Pattern.
	 * 
	 * @param propertyName
	 * @return Pattern
	 */
	public Pattern getPattern(final String propertyName)
	{
		String value = getString(propertyName);

		if (value == null)
		{
			return null;
		}

		return Pattern.compile(value);
	}

	/**
	 * Returns a Boolean.
	 * 
	 * @param propertyName
	 * @return Boolean
	 */
	public Boolean getBoolean(final String propertyName)
	{
		String value = getString(propertyName);

		if (value == null)
		{
			return null;
		}

		return Boolean.parseBoolean(value);
	}
}
