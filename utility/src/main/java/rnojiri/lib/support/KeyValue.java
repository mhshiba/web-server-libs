package rnojiri.lib.support;

/**
 * A generic key/value class.
 * 
 * @author rnojiri
 *
 * @param <K>
 * @param <V>
 */
public class KeyValue<K, V>
{
	private final K key;
	
	private final V value;

	/**
	 * @param key
	 * @param value
	 */
	public KeyValue(K key, V value)
	{
		this.key = key;
		this.value = value;
	}

	/**
	 * @return the key
	 */
	public K getKey()
	{
		return key;
	}

	/**
	 * @return the value
	 */
	public V getValue()
	{
		return value;
	}
}