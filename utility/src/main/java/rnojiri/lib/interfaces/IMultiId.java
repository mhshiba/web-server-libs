package rnojiri.lib.interfaces;

/**
 * Object with both internal and external ids. 
 * 
 * @author rnojiri
 *
 * @param <I>
 * @param <E>
 */
public interface IMultiId<I, E>
{
	/**
	 * Returns the internal ID.
	 * 
	 * @return I
	 */
	I getInternalId();
	
	/**
	 * Returns the external ID.
	 * 
	 * @return E
	 */
	E getExternalId();
}
