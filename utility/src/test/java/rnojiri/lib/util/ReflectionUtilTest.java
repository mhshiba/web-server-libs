package rnojiri.lib.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test the ReflectionUtil class.
 * 
 * @author rnojiri
 */
public class ReflectionUtilTest
{
	@Test
	public void testClassAnnotationInList() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		Assert.assertTrue(ReflectionUtil.isAnnotationPresent(TestClassMultipleAnnotations.class, ClassAnnotation1.class));
		Assert.assertTrue(ReflectionUtil.isAnnotationPresent(TestClassMultipleAnnotations.class, ClassAnnotation2.class));
		Assert.assertTrue(ReflectionUtil.isAnnotationPresent(TestClassMultipleAnnotations.class, ClassAnnotation3.class));
		Assert.assertFalse(ReflectionUtil.isAnnotationPresent(TestClassMultipleAnnotations.class, ClassAnnotation4.class));
		
		Assert.assertFalse(ReflectionUtil.isAnnotationPresent(TestClassMultipleAnnotations.class, MethodAnnotation1.class));
		Assert.assertFalse(ReflectionUtil.isAnnotationPresent(TestClassMultipleAnnotations.class, MethodAnnotation2.class));
		Assert.assertFalse(ReflectionUtil.isAnnotationPresent(TestClassMultipleAnnotations.class, MethodAnnotation3.class));
	}
	
	@Test
	public void testMethodAnnotationInList() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
	{
		Method method = TestClassMultipleAnnotations.class.getMethod("testMethod");
		
		Assert.assertTrue(ReflectionUtil.isAnnotationPresent(method, MethodAnnotation1.class));
		Assert.assertFalse(ReflectionUtil.isAnnotationPresent(method, MethodAnnotation2.class));
		Assert.assertTrue(ReflectionUtil.isAnnotationPresent(method, MethodAnnotation3.class));
		
		Assert.assertFalse(ReflectionUtil.isAnnotationPresent(method, ClassAnnotation1.class));
		Assert.assertFalse(ReflectionUtil.isAnnotationPresent(method, ClassAnnotation2.class));
		Assert.assertFalse(ReflectionUtil.isAnnotationPresent(method, ClassAnnotation3.class));
		Assert.assertFalse(ReflectionUtil.isAnnotationPresent(method, ClassAnnotation4.class));
	}
	
	@ClassAnnotation1
	@ClassAnnotation2
	@ClassAnnotation3
	private class TestClassMultipleAnnotations
	{
		@MethodAnnotation1
		@MethodAnnotation3
		public void testMethod() {};
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	private @interface ClassAnnotation1
	{
		
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	private @interface ClassAnnotation2
	{
		
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	private @interface ClassAnnotation3
	{
		
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	private @interface ClassAnnotation4
	{
		
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	private @interface MethodAnnotation1
	{
		
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	private @interface MethodAnnotation2
	{
		
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	private @interface MethodAnnotation3
	{
		
	}
}
