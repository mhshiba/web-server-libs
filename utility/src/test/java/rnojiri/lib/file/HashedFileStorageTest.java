package rnojiri.lib.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * Tests the file storage.
 * 
 * @author rnojiri
 */
public class HashedFileStorageTest
{
	/**
	 * Writes a new test file.
	 * 
	 * @param filePath
	 * @param content
	 * @throws IOException
	 */
	private void createTestFile(String filePath, String content) throws IOException
	{
		File file = new File(filePath);
		
		if (!file.exists())
		{
			file.createNewFile();
		}
		else
		{
			file.delete();
		}
		
		BufferedWriter bw = null;
		IOException exception = null;

		try
		{
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			
			bw = new BufferedWriter(fw);
			bw.write(content);
		}
		catch(IOException e)
		{
			exception = e;
		}
		finally 
		{
			if(bw != null)
			{
				bw.close();
			}
		}
		
		if(exception != null)
		{
			throw exception;
		}
	}
	
	@Test
	public void test1HexFunction() throws Exception
	{
		HashedFileStorage fileStorage = new HashedFileStorage("/tmp/hashed/", 16);
		
		Assert.assertEquals("00", fileStorage.toHex((byte)0));
		Assert.assertEquals("05", fileStorage.toHex((byte)5));
		Assert.assertEquals("0A", fileStorage.toHex((byte)10));
		Assert.assertEquals("0F", fileStorage.toHex((byte)15));
		Assert.assertEquals("10", fileStorage.toHex((byte)16));
		Assert.assertEquals("FF", fileStorage.toHex((byte)255));
	}

	@Test
	public void test2HashDirectory() throws Exception
	{
		InputStream inputStream = null;
		Exception exception = null;
		
		try
		{
			String filePath = "/tmp/testFile.tmp";
			
			createTestFile(filePath, "test");
	
			HashedFileStorage fileStorage = new HashedFileStorage("/tmp/hashed/", 16);
			
			inputStream = new FileInputStream("/tmp/testFile.tmp");
			
			FileProcessedData processedData = fileStorage.processInputStream(inputStream);
			
			byte[] expectedMd5Sum = {9, -113, 107, -51, 70, 33, -45, 115, -54, -34, 78, -125, 38, 39, -76, -10};
	
			for(int i=0; i<expectedMd5Sum.length; i++)
			{
				Assert.assertEquals(expectedMd5Sum[i], processedData.md5sum[i]);
			}
			
			Assert.assertEquals("/tmp/hashed/09/8F/6B/CD/46/21/D3/73/CA/DE/4E/83/26/27/B4/F6", fileStorage.buildMd5Path(processedData.md5sum).toString());
		}
		catch(Exception e)
		{
			exception = e;
		}
		finally
		{
			if(inputStream != null)
			{
				inputStream.close();
			}
		}
		
		if(exception != null)
		{
			throw exception;
		}
	}
	
	@Test
	public void test3FileSave() throws Exception
	{
		InputStream inputStream = null;
		Exception exception = null;
		
		try
		{
			FileUtils.deleteDirectory(new File("/tmp/hashed/"));
			
			inputStream = new FileInputStream("/tmp/testFile.tmp");
			HashedFileStorage fileStorage = new HashedFileStorage("/tmp/hashed/", 16);
			FileProcessedData fileProcessedData = fileStorage.processInputStream(inputStream);
			FileOpDescriptor descriptor = fileStorage.save("testFileCopy.tmp", fileProcessedData);
			
			Assert.assertNotNull(descriptor);
			Assert.assertEquals(new Long(4), new Long(descriptor.numBytes));
			
			File file = new File("/tmp/hashed/09/8F/6B/CD/46/21/D3/73/CA/DE/4E/83/26/27/B4/F6/testFileCopy.tmp");
			Assert.assertTrue(file.exists());
		}
		catch(Exception e)
		{
			exception = e;
		}
		finally
		{
			if(inputStream != null)
			{
				inputStream.close();
			}
		}
		
		if(exception != null)
		{
			throw exception;
		}
	}
	
	/**
	 * Tests a depth.
	 * 
	 * @param depth
	 */
	private void testInvalidDepth(int depth)
	{
		try
		{
			new HashedFileStorage("/tmp/hashed/", depth);
		}
		catch(Exception e)
		{
			Assert.assertEquals(InvalidDepthException.class, e.getClass());
		}
	}
	
	@Test
	public void test4InvalidDepths() throws NoSuchAlgorithmException, IOException
	{
		testInvalidDepth(-1);
		testInvalidDepth(0);
		testInvalidDepth(3);
		testInvalidDepth(5);
		testInvalidDepth(7);
		testInvalidDepth(9);
		testInvalidDepth(10);
		testInvalidDepth(11);
		testInvalidDepth(12);
		testInvalidDepth(13);
		testInvalidDepth(14);
		testInvalidDepth(15);
	}
	
	@Test
	public void test5NoDepth() throws Exception
	{
		InputStream inputStream = null;
		Exception exception = null;
		
		try
		{
			FileUtils.deleteDirectory(new File("/tmp/hashed/"));
			
			createTestFile("/tmp/testFile.tmp", "test");
			inputStream = new FileInputStream("/tmp/testFile.tmp");
			HashedFileStorage fileStorage = new HashedFileStorage("/tmp/hashed/", 1);
			FileProcessedData fileProcessedData = fileStorage.processInputStream(inputStream);
			FileOpDescriptor descriptor = fileStorage.save("testFileCopy.tmp", fileProcessedData);
			
			Assert.assertEquals(Paths.get("/tmp/hashed/098F6BCD4621D373CADE4E832627B4F6/testFileCopy.tmp"), descriptor.path);
		}
		catch(Exception e)
		{
			exception = e;
		}
		finally
		{
			if(inputStream != null)
			{
				inputStream.close();
			}
		}
		
		if(exception != null)
		{
			throw exception;
		}
	}
	
	@Test
	public void test6OtherDepths() throws Exception
	{
		InputStream inputStream = null;
		Exception exception = null;
		
		try
		{
			FileUtils.deleteDirectory(new File("/tmp/hashed/"));
			
			createTestFile("/tmp/testFile.tmp", "test");
			inputStream = new FileInputStream("/tmp/testFile.tmp");
			HashedFileStorage fileStorage = new HashedFileStorage("/tmp/hashed/", 2);
			FileProcessedData fileProcessedData = fileStorage.processInputStream(inputStream);
			FileOpDescriptor descriptor = fileStorage.save("testFileCopy.tmp", fileProcessedData);
			
			File dir1 = new File("/tmp/hashed/098F6BCD4621D373");
			
			Assert.assertTrue(dir1.exists());
			
			File dir2 = new File("/tmp/hashed/098F6BCD4621D373/CADE4E832627B4F6"); 
			
			Assert.assertTrue(dir2.exists());
			
			Assert.assertEquals(Paths.get("/tmp/hashed/098F6BCD4621D373/CADE4E832627B4F6/testFileCopy.tmp"), descriptor.path);
		}
		catch(Exception e)
		{
			exception = e;
		}
		finally
		{
			if(inputStream != null)
			{
				inputStream.close();
			}
		}
		
		if(exception != null)
		{
			throw exception;
		}
	}
}
