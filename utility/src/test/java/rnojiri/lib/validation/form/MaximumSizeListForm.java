package rnojiri.lib.validation.form;

import java.util.List;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.MaximumSize;

/**
 * Maximum size form.
 * 
 * @author rnojiri
 */
public class MaximumSizeListForm implements Validable
{
	private List<String> words;
	
	public MaximumSizeListForm()
	{
		;
	}

	/**
	 * @return the words
	 */
	@MaximumSize(4)
	public List<String> getWords()
	{
		return words;
	}

	/**
	 * @param words the words to set
	 */
	public void setWords(List<String> words)
	{
		this.words = words;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
