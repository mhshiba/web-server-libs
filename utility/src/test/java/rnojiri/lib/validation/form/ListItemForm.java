package rnojiri.lib.validation.form;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.RequiredParam;

/**
 * A sub form item.
 * 
 * @author rnojiri
 */
public class ListItemForm implements Validable
{
	private String item;
	
	public ListItemForm()
	{
		;
	}

	/**
	 * @return the item
	 */
	@RequiredParam
	public String getItem()
	{
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(String item)
	{
		this.item = item;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
