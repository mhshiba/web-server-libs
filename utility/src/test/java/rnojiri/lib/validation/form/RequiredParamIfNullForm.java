package rnojiri.lib.validation.form;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.RequiredParamIf;

/**
 * A form to test RequiredParamIf.
 * 
 * @author rnojiri
 */
public class RequiredParamIfNullForm implements Validable
{
    private String userTelephoneNumber;
      
    private String userMobileNumber;
	
	public RequiredParamIfNullForm()
	{
		;
	}

	/**
	 * @return the userTelephoneNumber
	 */
	@RequiredParamIf(parameter="userMobileNumber", value="")
	public String getUserTelephoneNumber()
	{
		return userTelephoneNumber;
	}

	/**
	 * @param userTelephoneNumber the userTelephoneNumber to set
	 */
	public void setUserTelephoneNumber(String userTelephoneNumber)
	{
		this.userTelephoneNumber = userTelephoneNumber;
	}

	/**
	 * @return the userMobileNumber
	 */
	@RequiredParamIf(parameter="userTelephoneNumber", value="")
	public String getUserMobileNumber()
	{
		return userMobileNumber;
	}

	/**
	 * @param userMobileNumber the userMobileNumber to set
	 */
	public void setUserMobileNumber(String userMobileNumber)
	{
		this.userMobileNumber = userMobileNumber;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
