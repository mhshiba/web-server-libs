package rnojiri.lib.validation.form;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.MinimumSize;

/**
 * Minimum size form.
 * 
 * @author rnojiri
 */
public class MinimumSizeForm implements Validable
{
	private String word;
	
	private Integer intNumber;
	
	private Float floatNumber;
	
	public MinimumSizeForm()
	{
		;
	}

	/**
	 * @return the word
	 */
	@MinimumSize(5)
	public String getWord()
	{
		return word;
	}

	/**
	 * @param word the word to set
	 */
	public void setWord(String word)
	{
		this.word = word;
	}
	
	/**
	 * @return the intNumber
	 */
	@MinimumSize(5)
	public Integer getIntNumber()
	{
		return intNumber;
	}

	/**
	 * @param intNumber the intNumber to set
	 */
	public void setIntNumber(Integer intNumber)
	{
		this.intNumber = intNumber;
	}

	/**
	 * @return the floatNumber
	 */
	@MinimumSize(5)
	public Float getFloatNumber()
	{
		return floatNumber;
	}

	/**
	 * @param floatNumber the floatNumber to set
	 */
	public void setFloatNumber(Float floatNumber)
	{
		this.floatNumber = floatNumber;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
