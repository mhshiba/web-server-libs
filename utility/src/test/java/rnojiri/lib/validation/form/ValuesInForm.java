package rnojiri.lib.validation.form;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.ValueIn;

/**
 * Values in form.
 * 
 * @author rnojiri
 */
public class ValuesInForm implements Validable
{
	private String option;

	private Integer optionWithNumbers;

	private int optionWithNativeNumbers;

	public ValuesInForm()
	{
		;
	}

	/**
	 * @return the option
	 */
	@ValueIn({ "A", "B", "C" })
	public String getOption()
	{
		return option;
	}

	/**
	 * @param option
	 *            the option to set
	 */
	public void setOption(String option)
	{
		this.option = option;
	}

	/**
	 * @return the optionWithNumbers
	 */
	@ValueIn({ "1", "2", "3" })
	public Integer getOptionWithNumbers()
	{
		return optionWithNumbers;
	}

	/**
	 * @param optionWithNumbers
	 *            the optionWithNumbers to set
	 */
	public void setOptionWithNumbers(Integer optionWithNumbers)
	{
		this.optionWithNumbers = optionWithNumbers;
	}

	/**
	 * @return the optionWithNativeNumbers
	 */
	@ValueIn({ "4", "5", "6" })
	public int getOptionWithNativeNumbers()
	{
		return optionWithNativeNumbers;
	}

	/**
	 * @param optionWithNativeNumbers
	 *            the optionWithNativeNumbers to set
	 */
	public void setOptionWithNativeNumbers(int optionWithNativeNumbers)
	{
		this.optionWithNativeNumbers = optionWithNativeNumbers;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
