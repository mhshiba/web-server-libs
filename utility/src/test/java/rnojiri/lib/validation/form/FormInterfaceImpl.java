package rnojiri.lib.validation.form;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;

/**
 * The implementation of FormInterface.
 * 
 * @author rnojiri
 */
public class FormInterfaceImpl implements TestFormInterface
{
	private String required;
	
	private String number;
	
	@Override
	public String getRequired()
	{
		return required;
	}

	@Override
	public String getNumber()
	{
		// TODO Auto-generated method stub
		return number;
	}

	/**
	 * @param required the required to set
	 */
	public void setRequired(String required)
	{
		this.required = required;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number)
	{
		this.number = number;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
