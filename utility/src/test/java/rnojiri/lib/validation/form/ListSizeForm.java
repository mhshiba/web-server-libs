package rnojiri.lib.validation.form;

import java.util.List;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.MinimumSize;

/**
 * List size form.
 * 
 * @author rnojiri
 */
public class ListSizeForm implements Validable
{
	private List<ListItemForm> items;
	
	public ListSizeForm()
	{
		;
	}

	/**
	 * @return the items
	 */
	@MinimumSize(3)
	public List<ListItemForm> getItems()
	{
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(List<ListItemForm> items)
	{
		this.items = items;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
