package rnojiri.lib.validation.form;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.MaximumSize;

/**
 * Maximum size form.
 * 
 * @author rnojiri
 */
public class MaximumSizeForm implements Validable
{
	private String word;
	
	private Integer intNumber;
	
	private Float floatNumber;
	
	public MaximumSizeForm()
	{
		;
	}

	/**
	 * @return the word
	 */
	@MaximumSize(5)
	public String getWord()
	{
		return word;
	}

	/**
	 * @param word the word to set
	 */
	public void setWord(String word)
	{
		this.word = word;
	}

	/**
	 * @return the intNumber
	 */
	@MaximumSize(5)
	public Integer getIntNumber()
	{
		return intNumber;
	}

	/**
	 * @param intNumber the intNumber to set
	 */
	public void setIntNumber(Integer intNumber)
	{
		this.intNumber = intNumber;
	}

	/**
	 * @return the floatNumber
	 */
	@MaximumSize(5)
	public Float getFloatNumber()
	{
		return floatNumber;
	}

	/**
	 * @param floatNumber the floatNumber to set
	 */
	public void setFloatNumber(Float floatNumber)
	{
		this.floatNumber = floatNumber;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
