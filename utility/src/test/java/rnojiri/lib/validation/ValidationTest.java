package rnojiri.lib.validation;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.ValidationException;

import org.junit.Test;

import rnojiri.lib.validation.Validator;
import rnojiri.lib.validation.ValidationError;
import rnojiri.lib.validation.form.FormInterfaceImpl;
import rnojiri.lib.validation.form.ListItemForm;
import rnojiri.lib.validation.form.ListParamForm;
import rnojiri.lib.validation.form.ListSizeForm;
import rnojiri.lib.validation.form.MaximumSizeForm;
import rnojiri.lib.validation.form.MaximumSizeListForm;
import rnojiri.lib.validation.form.MinimumSizeForm;
import rnojiri.lib.validation.form.MinimumSizeListForm;
import rnojiri.lib.validation.form.RegexpForm;
import rnojiri.lib.validation.form.RequiredFieldsForm;
import rnojiri.lib.validation.form.RequiredParamIfForm;
import rnojiri.lib.validation.form.RequiredParamIfNullForm;
import rnojiri.lib.validation.form.RequiredParamIfWithOptionalForm;
import rnojiri.lib.validation.form.TypeTestForm;
import rnojiri.lib.validation.form.ValuesInForm;
import rnojiri.lib.validation.form.ValuesInFormWithRequired;

/**
 * Tests the validator class.
 * 
 * @author rnojiri
 */
public class ValidationTest
{
	@Test
	public void testAllOptional()
	{
		TypeTestForm typeTestForm = new TypeTestForm();
		
		Map<String, String> invalidMap = Validator.verify(typeTestForm);
		
		assertEquals(0, invalidMap.size());
		
		assertEquals(0, typeTestForm.getIntVar());
		assertEquals(new Integer(2), typeTestForm.getIntCVar());
		assertEquals(0, typeTestForm.getFloatVar(), 0);
		assertEquals(4, typeTestForm.getFloatCVar(), 0);
		assertEquals(0, typeTestForm.getLongVar());
		assertEquals(new Long(6), typeTestForm.getLongCVar());
		assertEquals(Boolean.TRUE, typeTestForm.getBooleanCVar());
		assertEquals(false, typeTestForm.isBooleanVar());
		assertEquals("test", typeTestForm.getStringVar());
	}
	
	@Test
	public void testSettingOptional()
	{
		TypeTestForm typeTestForm = new TypeTestForm();
		typeTestForm.setIntVar(10);
		typeTestForm.setIntCVar(20);
		typeTestForm.setFloatVar(30.f);
		typeTestForm.setFloatCVar(40.f);
		typeTestForm.setLongVar(50);
		typeTestForm.setLongCVar(60L);
		typeTestForm.setBooleanVar(true);
		typeTestForm.setBooleanCVar(false);
		typeTestForm.setStringVar("changed");
		
		Map<String, String> invalidMap = Validator.verify(typeTestForm);
		
		assertEquals(0, invalidMap.size());
		
		assertEquals(10, typeTestForm.getIntVar());
		assertEquals(new Integer(20), typeTestForm.getIntCVar());
		assertEquals(30, typeTestForm.getFloatVar(), 0);
		assertEquals(40, typeTestForm.getFloatCVar(), 0);
		assertEquals(50, typeTestForm.getLongVar());
		assertEquals(new Long(60), typeTestForm.getLongCVar());
		assertEquals(Boolean.FALSE, typeTestForm.getBooleanCVar());
		assertEquals(true, typeTestForm.isBooleanVar());
		assertEquals("changed", typeTestForm.getStringVar());
	}
	
	@Test
	public void testRequiredFieldSuccess() throws ValidationException
	{
		RequiredFieldsForm requiredFieldsForm = new RequiredFieldsForm();
		requiredFieldsForm.setStringVar("test");
		requiredFieldsForm.setIntegerVar(1);
		requiredFieldsForm.setBooleanVar(true);
		
		Map<String, String> invalidMap = Validator.verify(requiredFieldsForm);
		
		assertEquals(0, invalidMap.size());
		
		assertEquals("test", requiredFieldsForm.getStringVar());
		assertEquals(new Integer(1), requiredFieldsForm.getIntegerVar());
		assertEquals(true, requiredFieldsForm.isBooleanVar());
	}
	
	@Test
	public void testRequiredFieldFailure()
	{
		RequiredFieldsForm requiredFieldsForm = new RequiredFieldsForm();
		
		Map<String, String> invalidMap = Validator.verify(requiredFieldsForm);
		
		assertEquals(2, invalidMap.size());
		
		assertEquals(ValidationError.EMPTY_REQUIRED_FIELD.name(), invalidMap.get("stringVar"));
		assertEquals(ValidationError.EMPTY_REQUIRED_FIELD.name(), invalidMap.get("integerVar"));
		
		assertEquals(false, requiredFieldsForm.isBooleanVar());
	}
	
	@Test
	public void testRegularExpressionSuccess()
	{
		RegexpForm regexpForm = new RegexpForm();
		regexpForm.setRegexpVar("abc@def.com");
		
		Map<String, String> invalidMap = Validator.verify(regexpForm);
		
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testRegularExpressionFailure()
	{
		RegexpForm regexpForm = new RegexpForm();
		regexpForm.setRegexpVar("abc@def");
		
		Map<String, String> invalidMap = Validator.verify(regexpForm);
		
		assertEquals(1, invalidMap.size());
		assertEquals(ValidationError.REGULAR_EXPRESSION_MISMATCH.name(), invalidMap.get("regexpVar"));
	}
	
	@Test
	public void testMinimumSizeSuccess()
	{
		MinimumSizeForm minimumSizeForm = new MinimumSizeForm();
		minimumSizeForm.setWord("success");
		minimumSizeForm.setIntNumber(5);
		minimumSizeForm.setFloatNumber(5.001f);
		
		Map<String, String> invalidMap = Validator.verify(minimumSizeForm);
		
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testMinimumSizeFailure()
	{
		MinimumSizeForm minimumSizeForm = new MinimumSizeForm();
		minimumSizeForm.setWord("suck");
		minimumSizeForm.setIntNumber(4);
		minimumSizeForm.setFloatNumber(4.99999f);
		
		Map<String, String> invalidMap = Validator.verify(minimumSizeForm);
		
		assertEquals(3, invalidMap.size());
		assertEquals(ValidationError.BELOW_MINIMUM_SIZE.name(), invalidMap.get("word"));
		assertEquals(ValidationError.BELOW_MINIMUM_SIZE.name(), invalidMap.get("intNumber"));
		assertEquals(ValidationError.BELOW_MINIMUM_SIZE.name(), invalidMap.get("floatNumber"));
		
		minimumSizeForm.setWord(null);
		minimumSizeForm.setIntNumber(null);
		minimumSizeForm.setFloatNumber(null);
		
		invalidMap = Validator.verify(minimumSizeForm);
		
		assertEquals(3, invalidMap.size());
		assertEquals(ValidationError.BELOW_MINIMUM_SIZE.name(), invalidMap.get("word"));
		assertEquals(ValidationError.BELOW_MINIMUM_SIZE.name(), invalidMap.get("intNumber"));
		assertEquals(ValidationError.BELOW_MINIMUM_SIZE.name(), invalidMap.get("floatNumber"));
	}
	
	@Test
	public void testMinimumSizeListSuccess()
	{
		List<String> words = new ArrayList<>();
		words.add("1");
		words.add("2");
		words.add("3");
		
		MinimumSizeListForm form = new MinimumSizeListForm();
		form.setWords(words);
		
		Map<String, String> invalidMap = Validator.verify(form);
		
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testMinimumSizeListFailure()
	{
		List<String> words = new ArrayList<>();
		words.add("1");
		
		MinimumSizeListForm form = new MinimumSizeListForm();
		form.setWords(words);
		
		Map<String, String> invalidMap = Validator.verify(form);
		
		assertEquals(1, invalidMap.size());
		assertEquals(ValidationError.BELOW_MINIMUM_SIZE.name(), invalidMap.get("words"));
		
		form.setWords(null);
		
		assertEquals(1, invalidMap.size());
		assertEquals(ValidationError.BELOW_MINIMUM_SIZE.name(), invalidMap.get("words"));
	}

	@Test
	public void testValuesInNativeNumbersSuccess()
	{
		ValuesInForm valuesInForm = new ValuesInForm();
		valuesInForm.setOptionWithNativeNumbers(4);
		
		Map<String, String> invalidMap = Validator.verify(valuesInForm);
		
		assertEquals(0, invalidMap.size());
		
		valuesInForm.setOptionWithNativeNumbers(5);
		
		invalidMap = Validator.verify(valuesInForm);
		
		assertEquals(0, invalidMap.size());
		
		valuesInForm.setOptionWithNativeNumbers(6);
		
		invalidMap = Validator.verify(valuesInForm);
		
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testValuesInNumbersSuccess()
	{
		ValuesInForm valuesInForm = new ValuesInForm();
		valuesInForm.setOptionWithNumbers(1);
		valuesInForm.setOptionWithNativeNumbers(5);
		
		Map<String, String> invalidMap = Validator.verify(valuesInForm);
		
		assertEquals(0, invalidMap.size());
		
		valuesInForm.setOptionWithNumbers(2);
		
		invalidMap = Validator.verify(valuesInForm);
		
		assertEquals(0, invalidMap.size());
		
		valuesInForm.setOptionWithNumbers(3);
		
		invalidMap = Validator.verify(valuesInForm);
		
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testValuesInStringSuccess()
	{
		ValuesInForm valuesInForm = new ValuesInForm();
		valuesInForm.setOption("B");
		valuesInForm.setOptionWithNativeNumbers(5);
		
		Map<String, String> invalidMap = Validator.verify(valuesInForm);
		
		assertEquals(0, invalidMap.size());
		
		valuesInForm.setOption("C");
		
		invalidMap = Validator.verify(valuesInForm);
		
		assertEquals(0, invalidMap.size());
		
		valuesInForm.setOption("A");
		
		invalidMap = Validator.verify(valuesInForm);
		
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testValuesInFailure()
	{
		ValuesInForm valuesInForm = new ValuesInForm();
		valuesInForm.setOption("D");
		valuesInForm.setOptionWithNativeNumbers(5);
		
		Map<String, String> invalidMap = Validator.verify(valuesInForm);
		
		assertEquals(1, invalidMap.size());
		assertEquals(ValidationError.INVALID_PARAMETER_OPTION.name(), invalidMap.get("option"));
	}
	
	@Test
	public void testValuesInNull()
	{
		ValuesInForm valuesInForm = new ValuesInForm();
		valuesInForm.setOption(null);
		valuesInForm.setOptionWithNativeNumbers(5);
		
		Map<String, String> invalidMap = Validator.verify(valuesInForm);

		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testFormListSuccess()
	{
		ListItemForm itemForm1 = new ListItemForm();
		itemForm1.setItem("item1");
		
		ListItemForm itemForm2 = new ListItemForm();
		itemForm2.setItem("item2");
		
		List<ListItemForm> list = new ArrayList<ListItemForm>(); 
		list.add(itemForm1);
		list.add(itemForm2);
		
		ListParamForm listParamForm = new ListParamForm();
		listParamForm.setParent("parent");
		listParamForm.setItems(list);
		
		Map<String, String> invalidMap = Validator.verify(listParamForm);
		
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testFormListFailure()
	{
		ListItemForm itemForm1 = new ListItemForm();
		itemForm1.setItem(null);
		
		ListItemForm itemForm2 = new ListItemForm();
		itemForm2.setItem(null);
		
		List<ListItemForm> list = new ArrayList<ListItemForm>(); 
		list.add(itemForm1);
		list.add(itemForm2);
		
		ListParamForm listParamForm = new ListParamForm();
		listParamForm.setParent("parent");
		listParamForm.setItems(list);
		
		Map<String, String> invalidMap = Validator.verify(listParamForm);
		
		assertEquals(2, invalidMap.size());
		assertEquals(ValidationError.EMPTY_REQUIRED_FIELD.name(), invalidMap.get("item[0]"));
		assertEquals(ValidationError.EMPTY_REQUIRED_FIELD.name(), invalidMap.get("item[1]"));
	}
	
	@Test
	public void testFormListSizeSuccess()
	{
		ListItemForm itemForm1 = new ListItemForm();
		itemForm1.setItem("A");
		
		ListItemForm itemForm2 = new ListItemForm();
		itemForm2.setItem("B");
		
		ListItemForm itemForm3 = new ListItemForm();
		itemForm3.setItem("C");
		
		List<ListItemForm> list = new ArrayList<ListItemForm>(); 
		list.add(itemForm1);
		list.add(itemForm2);
		list.add(itemForm3);
		
		ListSizeForm listSizeForm = new ListSizeForm();
		listSizeForm.setItems(list);
		
		Map<String, String> invalidMap = Validator.verify(listSizeForm);
		
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testFormListSizeFailure()
	{
		ListItemForm itemForm1 = new ListItemForm();
		itemForm1.setItem("A");
		
		ListItemForm itemForm2 = new ListItemForm();
		itemForm2.setItem("B");
		
		List<ListItemForm> list = new ArrayList<ListItemForm>(); 
		list.add(itemForm1);
		list.add(itemForm2);
		
		ListSizeForm listSizeForm = new ListSizeForm();
		listSizeForm.setItems(list);
		
		Map<String, String> invalidMap = Validator.verify(listSizeForm);
		
		assertEquals(1, invalidMap.size());
		assertEquals(ValidationError.BELOW_MINIMUM_SIZE.name(), invalidMap.get("items"));
	}
	
	@Test
	public void testFormListSizeNull()
	{
		ListSizeForm listSizeForm = new ListSizeForm();
		listSizeForm.setItems(null);
		
		Map<String, String> invalidMap = Validator.verify(listSizeForm);
		
		assertEquals(1, invalidMap.size());
		assertEquals(ValidationError.BELOW_MINIMUM_SIZE.name(), invalidMap.get("items"));
	}
	
	@Test
	public void testValuesInFormWithRequired()
	{
		ValuesInFormWithRequired form = new ValuesInFormWithRequired();
		
		Map<String, String> invalidMap = Validator.verify(form);
		assertEquals(1, invalidMap.size());
		assertEquals(ValidationError.EMPTY_REQUIRED_FIELD.name(), invalidMap.get("option"));
	}
	
	@Test
	public void testConditionalValuesAllNull()
	{
		RequiredParamIfForm form = new RequiredParamIfForm();
		
		Map<String, String> invalidMap = Validator.verify(form);
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testConditionalValuesString()
	{
		RequiredParamIfForm form = new RequiredParamIfForm();
		
		form.setType("notRequired");
		
		Map<String, String> invalidMap = Validator.verify(form);
		assertEquals(0, invalidMap.size());
		
		form.setType("required");
		
		invalidMap = Validator.verify(form);
		assertEquals(1, invalidMap.size());
		assertEquals(ValidationError.EMPTY_REQUIRED_FIELD.name(), invalidMap.get("requiredValue"));
		
		form.setRequiredValue("Some Value");
		
		invalidMap = Validator.verify(form);
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testConditionalValuesInteger()
	{
		RequiredParamIfForm form = new RequiredParamIfForm();
		
		form.setIntType(2);
		
		Map<String, String> invalidMap = Validator.verify(form);
		assertEquals(0, invalidMap.size());
		
		form.setIntType(5);
		
		invalidMap = Validator.verify(form);
		assertEquals(1, invalidMap.size());
		assertEquals(ValidationError.EMPTY_REQUIRED_FIELD.name(), invalidMap.get("requiredIntegerValue"));
		
		form.setRequiredIntegerValue(5);
		
		invalidMap = Validator.verify(form);
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testConditionalValuesWithDefaultValue()
	{
		RequiredParamIfWithOptionalForm form = new RequiredParamIfWithOptionalForm();
		
		Map<String, String> invalidMap = Validator.verify(form);
		assertEquals(1, invalidMap.size());
		assertEquals(ValidationError.EMPTY_REQUIRED_FIELD.name(), invalidMap.get("requiredValue"));
		
		form.setRequiredValue("test");
		
		invalidMap = Validator.verify(form);
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testMaximumSizeSuccess()
	{
		MaximumSizeForm form = new MaximumSizeForm();
		form.setWord("teste");
		form.setIntNumber(5);
		form.setFloatNumber(4.99f);
		
		Map<String, String> invalidMap = Validator.verify(form);
		
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testMaximumSizeFailure()
	{
		MaximumSizeForm form = new MaximumSizeForm();
		form.setWord("paralelepipedo");
		form.setIntNumber(10);
		form.setFloatNumber(5.001f);
		
		Map<String, String> invalidMap = Validator.verify(form);
		
		assertEquals(3, invalidMap.size());
		assertEquals(ValidationError.ABOVE_MAXIMUM_SIZE.name(), invalidMap.get("word"));
		assertEquals(ValidationError.ABOVE_MAXIMUM_SIZE.name(), invalidMap.get("intNumber"));
		assertEquals(ValidationError.ABOVE_MAXIMUM_SIZE.name(), invalidMap.get("floatNumber"));
		
		form.setWord(null);
		form.setIntNumber(null);
		form.setFloatNumber(null);
		
		invalidMap = Validator.verify(form);
		
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testMaximumSizeListSuccess()
	{
		List<String> words = new ArrayList<>();
		words.add("1");
		words.add("2");
		words.add("3");
		words.add("4");
		
		MaximumSizeListForm form = new MaximumSizeListForm();
		form.setWords(words);
		
		Map<String, String> invalidMap = Validator.verify(form);
		
		assertEquals(0, invalidMap.size());
	}
	
	@Test
	public void testMaximumSizeListFailure()
	{
		List<String> words = new ArrayList<>();
		words.add("1");
		words.add("2");
		words.add("3");
		words.add("4");
		words.add("5");
		
		MaximumSizeListForm form = new MaximumSizeListForm();
		form.setWords(words);
		
		Map<String, String> invalidMap = Validator.verify(form);
		
		assertEquals(1, invalidMap.size());
		assertEquals(ValidationError.ABOVE_MAXIMUM_SIZE.name(), invalidMap.get("words"));
	}
	
	@Test
	public void testRequireIfNull()
	{
		RequiredParamIfNullForm form = new RequiredParamIfNullForm();
		form.setUserMobileNumber("011973381560");
		
		Map<String, String> invalidMap = Validator.verify(form);
		
		assertEquals(0, invalidMap.size());

		form.setUserMobileNumber(null);
		form.setUserTelephoneNumber("011973381560");
		
		invalidMap = Validator.verify(form);
		
		assertEquals(0, invalidMap.size());
		
		form.setUserMobileNumber(null);
		form.setUserTelephoneNumber(null);
		
		invalidMap = Validator.verify(form);
		
		assertEquals(2, invalidMap.size());
	}
	
	@Test
	public void testFormInterface()
	{
		FormInterfaceImpl form = new FormInterfaceImpl();
		form.setRequired("test");
		form.setNumber("123456789");

		Map<String, String> invalidMap = Validator.verify(form);

		assertEquals(0, invalidMap.size());

		form.setRequired("1");
		form.setNumber("test");

		invalidMap = Validator.verify(form);

		assertEquals(1, invalidMap.size());
		
		form.setRequired(null);
		form.setNumber("test");

		invalidMap = Validator.verify(form);

		assertEquals(2, invalidMap.size());
		
		form.setRequired(null);
		form.setNumber(null);

		invalidMap = Validator.verify(form);

		assertEquals(2, invalidMap.size());
	}
}
