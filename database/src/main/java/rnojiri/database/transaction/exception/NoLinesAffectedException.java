package rnojiri.database.transaction.exception;

/**
 * Raised when no lines were affected in the database.
 * 
 * @author rnojiri
 */
public class NoLinesAffectedException extends Exception
{
	private static final long serialVersionUID = -8169827451968297603L;

	/**
	 * @param numLinesExpected
	 */
	public NoLinesAffectedException(int numLinesExpected)
	{
		super("Expected " + numLinesExpected + " lines inserted in database.");
	}
}
