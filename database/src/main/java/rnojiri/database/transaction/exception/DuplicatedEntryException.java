package rnojiri.database.transaction.exception;

import java.util.Arrays;

/**
 * Duplicated entry exception.
 * 
 * @author rnojiri
 */
public class DuplicatedEntryException extends Exception
{
	private static final long serialVersionUID = 6836504349631531043L;

	/**
	 * @param value
	 * @param columns
	 */
	public DuplicatedEntryException(Object value, String ... columns)
	{
		super("Entry with value \"" + String.valueOf(value) + "\" in column  \"" + Arrays.toString(columns) + "\" must be unique in database table.");
	}
}
