package rnojiri.database.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;

import rnojiri.database.transaction.AtomicTransaction;
import rnojiri.database.transaction.exception.AtomicTransactionException;
import rnojiri.database.transaction.exception.NoLinesAffectedException;
import rnojiri.lib.support.CallbackFunction;
import rnojiri.lib.support.ILoggable;

/**
 * Has useful database functions.
 * 
 * @author rnojiri
 */
@Component
public abstract class AbstractDatabaseService implements ILoggable
{
	@Autowired
	protected DataSourceTransactionManager dataSourceTransactionManager;
	
	/**
	 * Executes an atomic transaction.
	 * 
	 * @param callbackFunction
	 * @return T
	 * @throws AtomicTransactionException
	 */
	protected <T, E extends Exception> T executeAtomicTransaction(CallbackFunction<T, E> callbackFunction) throws AtomicTransactionException
	{
		AtomicTransaction<T, E> atomicTransaction = new AtomicTransaction<T, E>(dataSourceTransactionManager);
		
		return atomicTransaction.execute(callbackFunction);
	}
	
	/**
	 * Do a line operation in the database.
	 * 
	 * @param callbackFunction
	 * @param numLinesExpected
	 * @throws NoLinesAffectedException
	 */
	protected void tableLineOperation(CallbackFunction<Integer, Exception> callbackFunction, int numLinesExpected) throws NoLinesAffectedException
	{
		int numLines = 0;
		
		try
		{
			numLines = callbackFunction.execute();
		}
		catch(Exception e)
		{
			getLogger().error("No lines found when inserting or updating database table.", e);
		}
		
		if(numLines == 0)
		{
			throw new NoLinesAffectedException(numLinesExpected);
		}
	}
}
