package rnojiri.lib.test.database;

import java.beans.PropertyVetoException;
import java.io.IOException;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;

import ch.qos.logback.core.joran.spi.JoranException;
import rnojiri.lib.util.PropertiesManager;

/**
 * Configures the embedded mysql database.
 * 
 * @author rnojiri
 */
@Component
public abstract class AbstractTestDatabaseConfig
{
	private DataSource dataSource;
	
	private final String schemaBuildFile;
	
	private final String mappersLocation;
	
	private final String propertiesFilePath;
	
	/**
	 * @param schemaBuildFile
	 * @param mappersLocation
	 * @param logbackFilePath
	 * @param propertiesFilePath
	 * @throws JoranException
	 * @throws IOException 
	 */
	protected AbstractTestDatabaseConfig(String schemaBuildFile, String mappersLocation, String logbackFilePath, String propertiesFilePath) throws JoranException, IOException
	{
		this.schemaBuildFile = schemaBuildFile;
		this.mappersLocation = mappersLocation;
		this.propertiesFilePath = propertiesFilePath;
		
		if(this.propertiesFilePath != null)
		{
			PropertiesManager.getInstance().loadProperties(this.propertiesFilePath, true);
		}
		
		configureLogback(logbackFilePath);
	}
	
	/**
	 * Configures logback.
	 * 
	 * @param logbackFilePath
	 * @throws JoranException 
	 */
	private void configureLogback(String logbackFilePath) throws JoranException
	{
		if (logbackFilePath == null) return;
		
		System.setProperty("logback.configurationFile", logbackFilePath);
	}
	
	@Bean(destroyMethod = "shutdown")
	public DataSource dataSource() throws IOException
	{
		if(dataSource == null)
		{
			dataSource = new EmbeddedMysqlBuilder("test", "localhost", 13306, schemaBuildFile).build();
		}
		
		return dataSource;
	}
	
	@Bean
	public DataSourceTransactionManager transactionManager() throws PropertyVetoException, IOException
	{
		return new DataSourceTransactionManager(dataSource());
	}
	
	@Bean
	public SqlSessionFactory sqlSessionFactory() throws Exception
	{
		SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		sessionFactory.setConfigurationProperties(PropertiesManager.getInstance().getMainProperties());
		sessionFactory.setDataSource(dataSource());
		
		return sessionFactory.getObject();
	}
	
	@Bean
	public MapperScannerConfigurer mapperScannerConfigurer()
	{
		MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer(); 
		mapperScannerConfigurer.setBasePackage(mappersLocation);
		
		return mapperScannerConfigurer;
	}
}
