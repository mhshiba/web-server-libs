package rnojiri.lib.search.solr.segmentation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rnojiri.lib.search.segmentation.AbstractResultSegmentationAlgorithm;
import rnojiri.lib.search.segmentation.SegmentationAlgorithmFilter;

/**
 * The credit card segmentation filter algorithm.
 * 
 * Principal Segment - is defined by benefits selected
 * Top    - has the two selected benefits ordered by the number of benefits.
 * Middle - has the one of the selected benefits ordered by the number of benefits.
 * Bottom - has no selected benefits ordered by the number of benefits.
 * 
 * @author rnojiri
 */
public class CreditCardResultSegmentationAlgorithm extends AbstractResultSegmentationAlgorithm<CreditCardDocument, CreditCardSearchParameters>
{
	private static final int FIRST_FILTER_CATEGORIES = 2;
	
	private static final int SECOND_FILTER_CATEGORIES = 1;
	
	private static final BenefitTypesAndAnnualFeeComparator BENEFIT_TYPES_AND_ANNUAL_FEE_COMPARATOR = new BenefitTypesAndAnnualFeeComparator();
	
	private static final CreditCardWeightComparator CREDIT_CARD_WEIGHT_COMPARATOR = new CreditCardWeightComparator();
	
	/***
	 * 
	 */
	@Override
	protected List<SegmentationAlgorithmFilter<CreditCardDocument, CreditCardSearchParameters>> createSegmentationAlgorithmFilters()
	{
		List<SegmentationAlgorithmFilter<CreditCardDocument, CreditCardSearchParameters>> filterList = new ArrayList<>();
		
		filterList.add(mainSegment());
		filterList.add(createFirstSegment());
		filterList.add(createSecondSegment());
		filterList.add(createLastSegment());
		
		return filterList;
	}
	
	private SegmentationAlgorithmFilter<CreditCardDocument, CreditCardSearchParameters> mainSegment()
	{
		return 
				new SegmentationAlgorithmFilter<CreditCardDocument, CreditCardSearchParameters>()
				{

					@Override
					public boolean isEligible(CreditCardDocument item, CreditCardSearchParameters parameters)
					{
						Set<String> categoryTypeSet = extractCategoriesTypes(parameters);
						
						if (categoryTypeSet == null) return false;
						
						if(categoryTypeSet.contains(item.getTypeMainHighLight()))
						{
							return true;
						}
						
						return false;
					}

					@Override
					public List<CreditCardDocument> postOperation(List<CreditCardDocument> segmentedList)
					{						
						Collections.sort(segmentedList, CREDIT_CARD_WEIGHT_COMPARATOR);
						return segmentedList;
					}
				};
	}

	/**
	 * Checks if this item is eligible for this filter level.
	 * 
	 * @param item
	 * @param parameters
	 * @param numBenefitTypesExpected
	 * @return boolean
	 */
	private boolean innerIsEligible(CreditCardDocument item, CreditCardSearchParameters parameters, int numBenefitTypesExpected)
	{
		Set<String> categoryTypeSet = extractCategoriesTypes(parameters);
		
		if(categoryTypeSet == null) return false;
		
		int numRemoved = 0;
		
		for(String type : item.getTypeCreditCardBenefit())
		{
			if(categoryTypeSet.contains(type))
			{
				categoryTypeSet.remove(type);
				numRemoved++;
				
				if(categoryTypeSet.size() == 0) break;
			}
		}
		
		return numRemoved >= numBenefitTypesExpected;
	}
	
	/**
	 * Creates the first segmentation filter.
	 * 
	 * All credit cards with two selected benefits and ordered by number of benefits.
	 * 
	 * @return SegmentationAlgorithmFilter<CreditCardDocument, CreditCardSearchParameters>
	 */
	private SegmentationAlgorithmFilter<CreditCardDocument, CreditCardSearchParameters> createFirstSegment()
	{
		return 
			new SegmentationAlgorithmFilter<CreditCardDocument, CreditCardSearchParameters>()
			{
				@Override
				public List<CreditCardDocument> postOperation(List<CreditCardDocument> segmentedList)
				{
					Collections.sort(segmentedList, BENEFIT_TYPES_AND_ANNUAL_FEE_COMPARATOR);
					
					return segmentedList;
				}
				
				@Override
				public boolean isEligible(CreditCardDocument item, CreditCardSearchParameters parameters)
				{
					return innerIsEligible(item, parameters, FIRST_FILTER_CATEGORIES);
				}
			};
	}
	
	/**
	 * Creates the second segmentation filter.
	 * 
	 * All credit cards with one selected benefits and ordered by number of benefits.
	 * 
	 * @return SegmentationAlgorithmFilter<CreditCardDocument, CreditCardSearchParameters>
	 */
	private SegmentationAlgorithmFilter<CreditCardDocument, CreditCardSearchParameters> createSecondSegment()
	{
		return 
			new SegmentationAlgorithmFilter<CreditCardDocument, CreditCardSearchParameters>()
			{
				@Override
				public List<CreditCardDocument> postOperation(List<CreditCardDocument> segmentedList)
				{
					Collections.sort(segmentedList, BENEFIT_TYPES_AND_ANNUAL_FEE_COMPARATOR);
					
					return segmentedList;
				}
				
				@Override
				public boolean isEligible(CreditCardDocument item, CreditCardSearchParameters parameters)
				{
					return innerIsEligible(item, parameters, SECOND_FILTER_CATEGORIES);
				}
			};
	}
	
	/**
	 * Creates the last segmentation filter.
	 * 
	 * All the rest in.
	 * 
	 * @return SegmentationAlgorithmFilter<CreditCardDocument, CreditCardSearchParameters>
	 */
	private SegmentationAlgorithmFilter<CreditCardDocument, CreditCardSearchParameters> createLastSegment()
	{
		return 
			new SegmentationAlgorithmFilter<CreditCardDocument, CreditCardSearchParameters>()
			{
				@Override
				public List<CreditCardDocument> postOperation(List<CreditCardDocument> segmentedList)
				{
					Collections.sort(segmentedList, BENEFIT_TYPES_AND_ANNUAL_FEE_COMPARATOR);
					
					return segmentedList;
				}
				
				@Override
				public boolean isEligible(CreditCardDocument item, CreditCardSearchParameters parameters)
				{
					return true;
				}
			};
	}
	
	/**
	 * Extracts all selected categories.
	 * 
	 * @param parameters
	 * @return Set<String>
	 */ 
	private static Set<String> extractCategoriesTypes(CreditCardSearchParameters parameters)
	{
		List<String> categoryTypeRelevancies = parameters.getRelevancyBenefitType();
		
		if(categoryTypeRelevancies == null || categoryTypeRelevancies.size() == 0)
		{
			return null;
		}
		
		Set<String> categorySet = new HashSet<String>();
		
		for(String item : categoryTypeRelevancies)
		{
			categorySet.add(item);
		}
		
		return categorySet;
	}
	
	/**
	 * Sorts by number of benefit types and annual fee.
	 * 
	 * @author rnojiri
	 */
	private static class BenefitTypesAndAnnualFeeComparator implements Comparator<CreditCardDocument>
	{
		@Override
		public int compare(CreditCardDocument o1, CreditCardDocument o2)
		{
			Integer numTypeBenefits1 = new Integer(o1.getNumTypeCreditCardBenefits());
			Integer numTypeBenefits2 = new Integer(o2.getNumTypeCreditCardBenefits());
			
			int comparisonValue = -numTypeBenefits1.compareTo(numTypeBenefits2);
			
			if(comparisonValue != 0)
			{
				return comparisonValue;
			}
			else
			{
				Float annualFee1 = new Float(o1.getHolderFirstYearPriceAnnualFee());
				Float annualFee2 = new Float(o2.getHolderFirstYearPriceAnnualFee());
				
				return annualFee1.compareTo(annualFee2);
			}
		}
	}
	
	/**
	 * Sorts by benefit weight
	 * 
	 * @author rnojiri
	 */
	private static class CreditCardWeightComparator implements Comparator<CreditCardDocument>
	{
		@Override
		public int compare(CreditCardDocument o1, CreditCardDocument o2)
		{
			Float weightCreditCard1 = o1.getWeightCreditCard();
			Float weightCreditCard2 = o2.getWeightCreditCard();
			
			int comparisionValues = weightCreditCard2.compareTo(weightCreditCard1);
			
			if(comparisionValues != 0)
			{
				return comparisionValues;
			}
			else
			{				
				return BENEFIT_TYPES_AND_ANNUAL_FEE_COMPARATOR.compare(o1, o2); 
			}	
		}
	}
}
