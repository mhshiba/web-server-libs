package rnojiri.lib.search.solr.query;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/**
 * SolrQueryBuilder tests.
 * 
 * @author rnojiri
 */
public class SolrQueryBuilderTests
{
	@Test
	public void testEmptyQuery()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "";

		assertEquals(expectedQuery, builder.toString());
	}

	@Test
	public void testGetAll()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "*:*";

		assertEquals(expectedQuery, builder.all().toString());
	}

	@Test
	public void testOneField()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:value1";

		assertEquals(expectedQuery, builder.field("field1").value("value1").toString());
	}

	@Test
	public void testTwoFieldWithAND()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:value1 AND field2:value2";

		assertEquals(expectedQuery, builder.field("field1").value("value1").and().field("field2").value("value2").toString());
	}

	@Test
	public void testTwoFieldOmittingAND()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:value1 field2:value2";

		assertEquals(expectedQuery, builder.field("field1").value("value1").field("field2").value("value2").toString());
	}

	@Test
	public void testTwoFieldWithOR()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:value1 OR field2:value2";

		assertEquals(expectedQuery, builder.field("field1").value("value1").or().field("field2").value("value2").toString());

		expectedQuery = "field1:value1 OR field2:value2 OR field3:value3";

		assertEquals(expectedQuery, builder.or().field("field3").value("value3").toString());
	}

	@Test
	public void testIfFieldExists()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:[* TO *]";

		assertEquals(expectedQuery, builder.fieldExists("field1").toString());

		expectedQuery = "field1:[* TO *] AND field2:value2";

		assertEquals(expectedQuery, builder.and().field("field2").value("value2").toString());
	}

	@Test
	public void testIfFieldNotExists()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "-field1:[* TO *]";

		assertEquals(expectedQuery, builder.fieldNotExists("field1").toString());

		expectedQuery = "-field1:[* TO *] AND field2:value2";

		assertEquals(expectedQuery, builder.and().field("field2").value("value2").toString());
	}

	@Test
	public void testPhraseValue()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:este\\ valor\\ eh\\ uma\\ frase";

		assertEquals(expectedQuery, builder.field("field1").value("este valor eh uma frase").toString());

		expectedQuery = "field1:este\\ valor\\ eh\\ uma\\ frase AND field2:naoehfrase";

		assertEquals(expectedQuery, builder.and().field("field2").value("naoehfrase").toString());
	}

	@Test
	public void testReservedChars()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:\\+\\-\\&\\&\\|\\|\\!\\(\\)\\{\\}\\[\\]\\^\\\"\\~\\*\\?\\:\\\\";

		assertEquals(expectedQuery, builder.field("field1").value("+-&&||!(){}[]^\"~*?:\\").toString());
	}

	@Test
	public void testLessThan()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:{* TO 10}";

		assertEquals(expectedQuery, builder.field("field1").allLesserThan("10").toString());

		expectedQuery = "field1:{* TO 10} AND field2:value2";

		assertEquals(expectedQuery, builder.and().field("field2").value("value2").toString());
	}

	@Test
	public void testGreaterThan()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:{10 TO *}";

		assertEquals(expectedQuery, builder.field("field1").allGreaterThan("10").toString());

		expectedQuery = "field1:{10 TO *} AND field2:value2";

		assertEquals(expectedQuery, builder.and().field("field2").value("value2").toString());
	}

	@Test
	public void testLesserOrEqualsThan()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:[* TO 10]";

		assertEquals(expectedQuery, builder.field("field1").allLesserOrEqualsThan("10").toString());

		expectedQuery = "field1:[* TO 10] AND field2:value2";

		assertEquals(expectedQuery, builder.and().field("field2").value("value2").toString());
	}

	@Test
	public void testGreaterOrEqualsThan()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:[10 TO *]";

		assertEquals(expectedQuery, builder.field("field1").allGreaterOrEqualsThan("10").toString());

		expectedQuery = "field1:[10 TO *] AND field2:value2";

		assertEquals(expectedQuery, builder.and().field("field2").value("value2").toString());
	}

	@Test
	public void testFieldNegation()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "-field1:value1";

		assertEquals(expectedQuery, builder.mustNotHave().field("field1").value("value1").toString());

		expectedQuery = "-field1:value1 AND field2:value2";

		assertEquals(expectedQuery, builder.and().field("field2").value("value2").toString());
	}

	@Test
	public void testFieldObligation()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "+field1:value1";

		assertEquals(expectedQuery, builder.mustHave().field("field1").value("value1").toString());

		expectedQuery = "+field1:value1 AND field2:value2";

		assertEquals(expectedQuery, builder.and().field("field2").value("value2").toString());
	}

	@Test
	public void testSearchGroups()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:(+value1 -value2)";

		assertEquals(expectedQuery, builder.field("field1").openValueGroup().mustHave().value("value1").mustNotHave().value("value2").closeValueGroup().toString());
	}

	@Test
	public void testSimpleVariables()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:variableValue1";

		Map<String, String> values = new HashMap<String, String>();

		values.put("variable1", "variableValue1");

		assertEquals(expectedQuery, builder.field("field1").value("${variable1}").toString(values));

		expectedQuery = "field1:variableValue1 AND field2:variableValue2";

		values.put("variable2", "variableValue2");

		assertEquals(expectedQuery, builder.and().field("field2").value("${variable2}").toString(values));
	}

	@Test
	public void testRangeVariablesAllLesserThan()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:{* TO variableValue1}";

		Map<String, String> values = new HashMap<String, String>();

		values.put("variable1", "variableValue1");

		assertEquals(expectedQuery, builder.field("field1").allLesserThan("${variable1}").toString(values));

		expectedQuery = "field1:{* TO variableValue1} AND field2:{* TO variableValue2}";

		values.put("variable2", "variableValue2");

		assertEquals(expectedQuery, builder.and().field("field2").allLesserThan("${variable2}").toString(values));
	}

	@Test
	public void testRangeVariablesAllLesserOrEqualsThan()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:[* TO variableValue1]";

		Map<String, String> values = new HashMap<String, String>();

		values.put("variable1", "variableValue1");

		assertEquals(expectedQuery, builder.field("field1").allLesserOrEqualsThan("${variable1}").toString(values));

		expectedQuery = "field1:[* TO variableValue1] AND field2:[* TO variableValue2]";

		values.put("variable2", "variableValue2");

		assertEquals(expectedQuery, builder.and().field("field2").allLesserOrEqualsThan("${variable2}").toString(values));
	}

	@Test
	public void testRangeVariablesAllGreaterOrEqualsThan()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:[variableValue1 TO *]";

		Map<String, String> values = new HashMap<String, String>();

		values.put("variable1", "variableValue1");

		assertEquals(expectedQuery, builder.field("field1").allGreaterOrEqualsThan("${variable1}").toString(values));

		expectedQuery = "field1:[variableValue1 TO *] AND field2:[variableValue2 TO *]";

		values.put("variable2", "variableValue2");

		assertEquals(expectedQuery, builder.and().field("field2").allGreaterOrEqualsThan("${variable2}").toString(values));
	}

	@Test
	public void testRangeVariablesAllGreaterThan()
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:{variableValue1 TO *}";

		Map<String, String> values = new HashMap<String, String>();

		values.put("variable1", "variableValue1");

		assertEquals(expectedQuery, builder.field("field1").allGreaterThan("${variable1}").toString(values));

		expectedQuery = "field1:{variableValue1 TO *} AND field2:{variableValue2 TO *}";

		values.put("variable2", "variableValue2");

		assertEquals(expectedQuery, builder.and().field("field2").allGreaterThan("${variable2}").toString(values));
	}

	@Test
	public void testWithAnyValueArray() throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:(value1 OR value2 OR value3)";

		assertEquals(expectedQuery, builder.field("field1").withAnyValue("value1", "value2", "value3").toString());

		expectedQuery = "field1:(value1 OR value2 OR value3\\ and\\ 4)";

		builder = new SolrQueryBuilder();

		assertEquals(expectedQuery, builder.field("field1").withAnyValue("value1", "value2", "value3 and 4").toString());
	}

	@Test
	public void testWithAnyValueCollection() throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:(value1 OR value2 OR value3)";

		Collection<String> collection = new ArrayList<String>();
		collection.add("value1");
		collection.add("value2");
		collection.add("value3");

		assertEquals(expectedQuery, builder.field("field1").withAnyValue(String.class, collection).toString());

		expectedQuery = "field1:(value1 OR value2 OR value3\\ and\\ 4)";

		collection = new ArrayList<String>();
		collection.add("value1");
		collection.add("value2");
		collection.add("value3 and 4");

		builder = new SolrQueryBuilder();

		assertEquals(expectedQuery, builder.field("field1").withAnyValue("value1", "value2", "value3 and 4").toString());
	}
	
	@Test
	public void testWithAllValuesArray() throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:(value1 AND value2 AND value3)";

		assertEquals(expectedQuery, builder.field("field1").withAllValues("value1", "value2", "value3").toString());

		expectedQuery = "field1:(value1 AND value2 AND value3\\ and\\ 4)";

		builder = new SolrQueryBuilder();

		assertEquals(expectedQuery, builder.field("field1").withAllValues("value1", "value2", "value3 and 4").toString());
	}

	@Test
	public void testWithAllValuesCollection() throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();

		String expectedQuery = "field1:(value1 AND value2 AND value3)";

		Collection<String> collection = new ArrayList<String>();
		collection.add("value1");
		collection.add("value2");
		collection.add("value3");

		assertEquals(expectedQuery, builder.field("field1").withAllValues(String.class, collection).toString());

		expectedQuery = "field1:(value1 AND value2 AND value3\\ and\\ 4)";

		collection = new ArrayList<String>();
		collection.add("value1");
		collection.add("value2");
		collection.add("value3 and 4");

		builder = new SolrQueryBuilder();

		assertEquals(expectedQuery, builder.field("field1").withAllValues("value1", "value2", "value3 and 4").toString());
	}
	
	@Test
	public void testWithAnyInteger() throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();
		
		String expectedQuery = "field1:(1 AND 2 AND 3)";
		
		Collection<Integer> collection = new ArrayList<Integer>();
		collection.add(1);
		collection.add(2);
		collection.add(3);
		
		assertEquals(expectedQuery, builder.field("field1").withAllValues(Integer.class, collection).toString());

		builder = new SolrQueryBuilder();

		assertEquals(expectedQuery, builder.field("field1").withAllValues(1, 2, 3).toString());
	}
	
	@Test
	public void testWithAnyLong() throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException
	{
		SolrQueryBuilder builder = new SolrQueryBuilder();
		
		String expectedQuery = "field1:(1 AND 2 AND 3)";
		
		Collection<Long> collection = new ArrayList<Long>();
		collection.add(1L);
		collection.add(2L);
		collection.add(3L);
		
		assertEquals(expectedQuery, builder.field("field1").withAllValues(Long.class, collection).toString());

		builder = new SolrQueryBuilder();

		assertEquals(expectedQuery, builder.field("field1").withAllValues(1L, 2L, 3L).toString());
	}

	@Test
	public void testCopyConstructor()
	{
		SolrQueryBuilder facetQuery = new SolrQueryBuilder().field("field1").value("X").setLabel("Titulo da faceta");

		SolrQueryBuilder builder1 = new SolrQueryBuilder().field("field1").value("value1")
														  .and()
														  .field("field2").value("value2")
														  .setupFacets(1, 10, true)
														  .addSortBy("field2", true)
														  .addFacetField("field1")
														  .addFacetQuery(facetQuery)
														  .setLabel("Label do pai");

		SolrQueryBuilder builder2 = new SolrQueryBuilder(builder1);

		HashMap<String, String> parameters1 = extractParameters(builder1);

		HashMap<String, String> parameters2 = extractParameters(builder2);

		for (String param : parameters1.values())
		{

			assertEquals(param, parameters2.get(param));
		}

		assertEquals(builder1.getLabel(), builder2.getLabel());
	}
	
	@Test
	public void testCopyConstructorUsingVariables()
	{
		SolrQueryBuilder builder1 = new SolrQueryBuilder().field("field1").value("${variable1}")
														  .and()
														  .field("field2").value("${variable2}")
														  .setupFacets(1, -1, true)
														  .addFacetField("field1")
														  .addSortBy("field2", false);

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("variable1", "value1");
		map.put("variable2", "value2");

		String expectedQuery = "field1:value1 AND field2:value2";

		assertEquals(expectedQuery, builder1.toString(map));

		SolrQueryBuilder builder2 = new SolrQueryBuilder(builder1);

		assertEquals(expectedQuery, builder2.toString(map));

		assertEquals(builder1.getRawQuery(), builder2.getRawQuery());
	}

	@Test
	public void testDateObjects()
	{
		SolrQueryBuilder builder1 = new SolrQueryBuilder();

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2011);
		calendar.set(Calendar.MONTH, Calendar.OCTOBER);
		calendar.set(Calendar.DAY_OF_MONTH, 9);
		calendar.set(Calendar.HOUR_OF_DAY, 8);
		calendar.set(Calendar.MINUTE, 7);
		calendar.set(Calendar.SECOND, 6);

		String expectedQuery = "field1:2011-10-09T08:07:06Z";

		assertEquals(expectedQuery, builder1.field("field1").value(calendar.getTime()).toString());
	}

	@Test
	public void testClosedRangesDateObjects()
	{
		SolrQueryBuilder builder1 = new SolrQueryBuilder();

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2011);
		calendar.set(Calendar.MONTH, Calendar.OCTOBER);
		calendar.set(Calendar.DAY_OF_MONTH, 9);
		calendar.set(Calendar.HOUR_OF_DAY, 8);
		calendar.set(Calendar.MINUTE, 7);
		calendar.set(Calendar.SECOND, 6);

		String expectedQuery = "field1:[* TO 2011-10-09T08:07:06Z]";

		assertEquals(expectedQuery, builder1.field("field1").allLesserOrEqualsThan(calendar.getTime()).toString());

		builder1 = new SolrQueryBuilder();

		expectedQuery = "field1:[2011-10-09T08:07:06Z TO *]";

		assertEquals(expectedQuery, builder1.field("field1").allGreaterOrEqualsThan(calendar.getTime()).toString());

		builder1 = new SolrQueryBuilder();

		expectedQuery = "field1:[2011-10-09T08:07:06Z TO 2011-10-09T08:07:06Z]";

		assertEquals(expectedQuery, builder1.field("field1").allBetween(calendar.getTime(), calendar.getTime(), true).toString());
	}

	@Test
	public void testOpenRangesDateObjects()
	{
		SolrQueryBuilder builder1 = new SolrQueryBuilder();

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2011);
		calendar.set(Calendar.MONTH, Calendar.OCTOBER);
		calendar.set(Calendar.DAY_OF_MONTH, 9);
		calendar.set(Calendar.HOUR_OF_DAY, 8);
		calendar.set(Calendar.MINUTE, 7);
		calendar.set(Calendar.SECOND, 6);

		String expectedQuery = "field1:{* TO 2011-10-09T08:07:06Z}";

		assertEquals(expectedQuery, builder1.field("field1").allLesserThan(calendar.getTime()).toString());

		builder1 = new SolrQueryBuilder();

		expectedQuery = "field1:{2011-10-09T08:07:06Z TO *}";

		assertEquals(expectedQuery, builder1.field("field1").allGreaterThan(calendar.getTime()).toString());

		builder1 = new SolrQueryBuilder();

		expectedQuery = "field1:{2011-10-09T08:07:06Z TO 2011-10-09T08:07:06Z}";

		assertEquals(expectedQuery, builder1.field("field1").allBetween(calendar.getTime(), calendar.getTime(), false).toString());
	}

	@Test
	public void testLabeledFacetField()
	{
		SolrQueryBuilder builder1 = new SolrQueryBuilder();

		builder1.field("field1").value("value1").addFacetField(new LabeledValue("Nome do Label", "field1"));

		HashMap<String, String> parameters = extractParameters(builder1);

		String expectedParams[] = { "facet.field={!key='Nome do Label'}field1", "facet=true", "q=field1:value1" };

		for (String param : expectedParams)
		{
			assertEquals(param, parameters.get(param));
		}
	}

	@Test
	public void testMultiLabeledFacetFields()
	{
		SolrQueryBuilder builder1 = new SolrQueryBuilder();

		builder1.field("field1").value("value1").addFacetField(new LabeledValue("Nome do Label", "field1"), 
															   new LabeledValue("Nome do segundo Label", "field2"));

		HashMap<String, String> parameters = extractParameters(builder1);

		String expectedParams[] = { "facet.field={!key='Nome do Label'}field1", "facet=true", "q=field1:value1", "facet.field={!key='Nome do segundo Label'}field2" };

		for (String param : expectedParams)
		{
			assertEquals(param, parameters.get(param));
		}
	}

	@Test
	public void testMixedFacetFields()
	{
		SolrQueryBuilder builder1 = new SolrQueryBuilder();

		builder1.field("field1").value("value1").addFacetField(new LabeledValue("Nome do Label", "field1")).addFacetField("field2")
												.addFacetField(new LabeledValue("Nome do segundo Label", "field3"))
												.addFacetField("field4").field("field5").value("value5");

		HashMap<String, String> parameters = extractParameters(builder1);

		String expectedParams[] = { "facet.field={!key='Nome do Label'}field1", "facet.field={!key='Nome do segundo Label'}field3", "facet.field=field2", "facet.field=field4", "facet=true",
				"q=field1:value1 field5:value5" };

		for (String param : expectedParams)
		{
			assertEquals(param, parameters.get(param));
		}
	}

	/**
	 * Extracts all parameters.
	 * 
	 * @param builder
	 * @return HashMap<String, String>
	 */
	private HashMap<String, String> extractParameters(SolrQueryBuilder builder)
	{
		try
		{
			String array[] = URLDecoder.decode(builder.getRawQuery(), "utf-8").split("&");

			HashMap<String, String> parameters = new HashMap<String, String>();

			for (String item : array)
			{
				if (item.trim().length() > 0)
				{
					parameters.put(item, item);
				}
			}

			return parameters;

		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testSolrTime()
	{
		SolrQueryBuilder builder1 = new SolrQueryBuilder();

		String expectedQuery = "field1:NOW-2YEAR";

		assertEquals(expectedQuery, builder1.field("field1").value(new SolrTime().addValue(-2, SolrTime.Measure.YEAR)).toString());
	}

	@Test
	public void testSolrTimeRanges()
	{
		SolrQueryBuilder builder1 = new SolrQueryBuilder();

		String expectedQuery = "field1:[* TO NOW+7DAY]";

		assertEquals(expectedQuery, builder1.field("field1").allLesserOrEqualsThan(new SolrTime().addValue(7, SolrTime.Measure.DAY)).toString());

		builder1 = new SolrQueryBuilder();

		expectedQuery = "field1:[NOW-7DAY TO *]";

		assertEquals(expectedQuery, builder1.field("field1").allGreaterOrEqualsThan(new SolrTime().addValue(-7, SolrTime.Measure.DAY)).toString());

		builder1 = new SolrQueryBuilder();

		expectedQuery = "field1:[NOW-1YEAR TO NOW]";

		assertEquals(expectedQuery, builder1.field("field1").allBetween(new SolrTime().addValue(-1, SolrTime.Measure.YEAR), new SolrTime(), true).toString());
	}

	@Test
	public void testFuzzyValues()
	{
		SolrQueryBuilder builder1 = new SolrQueryBuilder();

		String expectedQuery = "field1:teste~0.15";

		float fuzzy = 0.15f;

		assertEquals(expectedQuery, builder1.field("field1").value("teste", fuzzy).toString());
	}

	@Test
	public void testFuzzyPhraseValues()
	{
		SolrQueryBuilder builder1 = new SolrQueryBuilder();

		String expectedQuery = "field1:teste\\ do\\ fuzzy~0.25";

		float fuzzy = 0.25f;

		assertEquals(expectedQuery, builder1.field("field1").value("teste do fuzzy", fuzzy).toString());
	}
	
	@Test
	public void testAppendQuery()
	{
		SolrQueryBuilder builder1 = new SolrQueryBuilder();
		builder1.field("field1").value("test1");
		
		SolrQueryBuilder builder2 = new SolrQueryBuilder();
		builder2.and();
		builder2.field("field2").value("test2");
		
		builder1.append(builder2);
		
		String expectedQuery = "field1:test1 AND field2:test2";
		
		assertEquals(expectedQuery, builder1.toString());
	}
	
	@Test
	public void testNumberRange()
	{
		String expected1 = "field1:[1 TO 10]";
		
		assertEquals(expected1, new SolrQueryBuilder().field("field1").allBetween(1, 10, true).toString());
		
		String expected2 = "field1:[* TO 10]";
		
		assertEquals(expected2, new SolrQueryBuilder().field("field1").allBetween(null, 10, true).toString());
		assertEquals(expected2, new SolrQueryBuilder().field("field1").allLesserOrEqualsThan(10).toString());
		
		String expected3 = "field1:[1 TO *]";
		
		assertEquals(expected3, new SolrQueryBuilder().field("field1").allBetween(1, null, true).toString());
		assertEquals(expected3, new SolrQueryBuilder().field("field1").allGreaterOrEqualsThan(1).toString());
		
		String expected4 = "field1:{1 TO 10}";
		
		assertEquals(expected4, new SolrQueryBuilder().field("field1").allBetween(1, 10, false).toString());
		
		String expected5 = "field1:{* TO 10}";
		
		assertEquals(expected5, new SolrQueryBuilder().field("field1").allBetween(null, 10, false).toString());
		assertEquals(expected5, new SolrQueryBuilder().field("field1").allLesserThan(10).toString());
		
		String expected6 = "field1:{1 TO *}";
		
		assertEquals(expected6, new SolrQueryBuilder().field("field1").allBetween(1, null, false).toString());
		assertEquals(expected6, new SolrQueryBuilder().field("field1").allGreaterThan(1).toString());
	}
}
