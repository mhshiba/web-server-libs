package rnojiri.lib.search.solr.comparison;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import rnojiri.lib.search.comparison.ComparisonGrid;
import rnojiri.lib.search.comparison.ComparisonGridResultReader;
import rnojiri.lib.search.comparison.ComparisonItem;

/**
 * Tests the ComparisonGrid class.
 * 
 * @author rnojiri
 */
public class ComparisonGridTest
{
	@Test
	public void testGridSize()
	{
		ComparisonGrid<String> comparisonGrid = new ComparisonGrid<String>(4);
		comparisonGrid.put("key1", new ComparisonItem<String>(1.0f, "description1"), 0);
		comparisonGrid.put("key2", new ComparisonItem<String>(2.0f, "description2"), 1);
		comparisonGrid.put("key3", new ComparisonItem<String>(3.0f, "description3"), 2);
		
		assertEquals(3, comparisonGrid.getNumLines());
		assertEquals(4, comparisonGrid.getNumColumns());
	}
	
	@Test
	public void testGridSizeNoNewLines()
	{
		ComparisonGrid<String> comparisonGrid = new ComparisonGrid<String>(3);
		comparisonGrid.put("key1", new ComparisonItem<String>(1.0f, "description1"), 0);
		comparisonGrid.put("key1", new ComparisonItem<String>(2.0f, "description2"), 0);
		comparisonGrid.put("key1", new ComparisonItem<String>(3.0f, "description3"), 0);
		
		assertEquals(1, comparisonGrid.getNumLines());
		assertEquals(3, comparisonGrid.getNumColumns());
	}
	
	@Test
	public void testGridOrder()
	{
		ComparisonGrid<String> comparisonGrid = new ComparisonGrid<String>(1);
		comparisonGrid.put("ckey1", new ComparisonItem<String>(1.0f, "description1"), 0);
		comparisonGrid.put("bkey2", new ComparisonItem<String>(2.0f, "description2"), 0);
		comparisonGrid.put("akey3", new ComparisonItem<String>(3.0f, "description3"), 0);
		
		ComparisonGridResultReader<String> reader = comparisonGrid.getComparisonGridResultReader();
		
		assertEquals("ckey1", reader.getKey(0));
		assertEquals("bkey2", reader.getKey(1));
		assertEquals("akey3", reader.getKey(2));
	}
	
	@Test
	public void testGridNulls()
	{
		ComparisonGrid<String> comparisonGrid = new ComparisonGrid<String>(3);
		comparisonGrid.put("key1", new ComparisonItem<String>(1.0f, "description1"), 0);
		comparisonGrid.put("key1", new ComparisonItem<String>(2.0f, "description2"), 2);
		comparisonGrid.put("key2", new ComparisonItem<String>(3.0f, "description3"), 1);
		comparisonGrid.put("key2", new ComparisonItem<String>(4.0f, "description4"), 2);
		
		ComparisonGridResultReader<String> reader = comparisonGrid.getComparisonGridResultReader();
		
		assertEquals("description1", reader.getValue(0, 0));
		assertEquals(null, reader.getValue(0, 1));
		assertEquals("description2", reader.getValue(0, 2));
		
		assertEquals(null, reader.getValue(1, 0));
		assertEquals("description3", reader.getValue(1, 1));
		assertEquals("description4", reader.getValue(1, 2));
	}
	
	@Test
	public void testGridLineBestChoice()
	{
		ComparisonGrid<String> comparisonGrid = new ComparisonGrid<String>(4);
		
		comparisonGrid.put("key1", new ComparisonItem<String>(1.0f, "description1"), 0);
		comparisonGrid.put("key1", new ComparisonItem<String>(2.0f, "description2"), 1);
		comparisonGrid.put("key1", new ComparisonItem<String>(10.0f, "description3"), 2);
		comparisonGrid.put("key1", new ComparisonItem<String>(3.0f, "description4"), 3);
		
		comparisonGrid.put("key2", new ComparisonItem<String>(20.0f, "description1"), 0);
		comparisonGrid.put("key2", new ComparisonItem<String>(2.0f, "description2"), 1);
		comparisonGrid.put("key2", new ComparisonItem<String>(1.0f, "description3"), 2);
		comparisonGrid.put("key2", new ComparisonItem<String>(3.0f, "description4"), 3);
		
		ComparisonGridResultReader<String> reader = comparisonGrid.getComparisonGridResultReader();
		
		assertEquals(false, reader.isTheBestChoice(0, 0));
		assertEquals(false, reader.isTheBestChoice(0, 1));
		assertEquals(true, reader.isTheBestChoice(0, 2));
		assertEquals(false, reader.isTheBestChoice(0, 3));
		
		assertEquals(true, reader.isTheBestChoice(1, 0));
		assertEquals(false, reader.isTheBestChoice(1, 1));
		assertEquals(false, reader.isTheBestChoice(1, 2));
		assertEquals(false, reader.isTheBestChoice(1, 3));
	}
	
	@Test
	public void testGridLineInverseBestChoice()
	{
		ComparisonGrid<String> comparisonGrid = new ComparisonGrid<String>(4, true);
		
		comparisonGrid.put("key1", new ComparisonItem<String>(1.0f, "description1"), 0);
		comparisonGrid.put("key1", new ComparisonItem<String>(2.0f, "description2"), 1);
		comparisonGrid.put("key1", new ComparisonItem<String>(10.0f, "description3"), 2);
		comparisonGrid.put("key1", new ComparisonItem<String>(3.0f, "description4"), 3);
		
		comparisonGrid.put("key2", new ComparisonItem<String>(20.0f, "description1"), 0);
		comparisonGrid.put("key2", new ComparisonItem<String>(2.0f, "description2"), 1);
		comparisonGrid.put("key2", new ComparisonItem<String>(1.0f, "description3"), 2);
		comparisonGrid.put("key2", new ComparisonItem<String>(3.0f, "description4"), 3);
		
		ComparisonGridResultReader<String> reader = comparisonGrid.getComparisonGridResultReader();
		
		assertEquals(true, reader.isTheBestChoice(0, 0));
		assertEquals(false, reader.isTheBestChoice(0, 1));
		assertEquals(false, reader.isTheBestChoice(0, 2));
		assertEquals(false, reader.isTheBestChoice(0, 3));
		
		assertEquals(false, reader.isTheBestChoice(1, 0));
		assertEquals(false, reader.isTheBestChoice(1, 1));
		assertEquals(true, reader.isTheBestChoice(1, 2));
		assertEquals(false, reader.isTheBestChoice(1, 3));
	}
	
	@Test
	public void testGridLineNoBestChoice()
	{
		ComparisonGrid<String> comparisonGrid = new ComparisonGrid<String>(4);
		
		comparisonGrid.put("key1", new ComparisonItem<String>(10.0f, "description1"), 0);
		comparisonGrid.put("key1", new ComparisonItem<String>(2.0f, "description2"), 1);
		comparisonGrid.put("key1", new ComparisonItem<String>(10.0f, "description3"), 2);
		comparisonGrid.put("key1", new ComparisonItem<String>(3.0f, "description4"), 3);
		
		comparisonGrid.put("key2", new ComparisonItem<String>(0.0f, "description1"), 0);
		comparisonGrid.put("key2", new ComparisonItem<String>(2.0f, "description2"), 1);
		comparisonGrid.put("key2", new ComparisonItem<String>(3.0f, "description3"), 2);
		comparisonGrid.put("key2", new ComparisonItem<String>(3.0f, "description4"), 3);
		
		ComparisonGridResultReader<String> reader = comparisonGrid.getComparisonGridResultReader();
		
		assertEquals(false, reader.isTheBestChoice(0, 0));
		assertEquals(false, reader.isTheBestChoice(0, 1));
		assertEquals(false, reader.isTheBestChoice(0, 2));
		assertEquals(false, reader.isTheBestChoice(0, 3));
		
		assertEquals(false, reader.isTheBestChoice(1, 0));
		assertEquals(false, reader.isTheBestChoice(1, 1));
		assertEquals(false, reader.isTheBestChoice(1, 2));
		assertEquals(false, reader.isTheBestChoice(1, 3));
	}
	
	@Test
	public void testEmptyGridLine()
	{
		ComparisonGrid<String> comparisonGrid = new ComparisonGrid<String>(3);
		
		ComparisonGridResultReader<String> reader = comparisonGrid.getComparisonGridResultReader();
		
		assertEquals(0, reader.getNumLines());
		assertEquals(3, reader.getNumColumns());
	}
	
	@Test
	public void testOutOfBoundsException()
	{
		ComparisonGrid<String> comparisonGrid = new ComparisonGrid<String>(2);
		
		comparisonGrid.put("key1", new ComparisonItem<String>(10.0f, "description1"), 0);
		
		ComparisonGridResultReader<String> reader = comparisonGrid.getComparisonGridResultReader();
		
		try
		{
			reader.getKey(2);
		}
		catch(Exception e)
		{
			assertEquals(true, e instanceof IndexOutOfBoundsException);
		}
		
		try
		{
			reader.getValue(2, 0);
		}
		catch(Exception e)
		{
			assertEquals(true, e instanceof IndexOutOfBoundsException);
		}
		
		try
		{
			reader.getValue(1, 3);
		}
		catch(Exception e)
		{
			assertEquals(true, e instanceof IndexOutOfBoundsException);
		}
	}
}
