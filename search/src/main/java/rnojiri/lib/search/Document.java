package rnojiri.lib.search;

import java.io.Serializable;

/**
 * A search engine document.
 * 
 * @author rnojiri
 */
public interface Document extends Serializable
{
	/**
	 * Returns the document id.
	 * 
	 * @return
	 */
	String getId();
}
