package rnojiri.lib.search.solr.server;

/**
 * Solr document modes.
 * 
 * @author rnojiri
 */
public enum SolrServerMode
{
	BINARY,
	XML
}