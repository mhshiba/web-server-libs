package rnojiri.lib.search.solr.server;

import java.net.MalformedURLException;

import org.apache.solr.client.solrj.impl.BinaryRequestWriter;
import org.apache.solr.client.solrj.impl.BinaryResponseParser;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.request.RequestWriter;

/**
 * A custom configuration Solr server.
 * 
 * @author rnojiri
 */
public class CustomHttpSolrServer extends HttpSolrClient
{
	private static final long serialVersionUID = 1388864693671486132L;

	private SolrServerMode indexingMode;
	
	private String name;

	/**
	 * @param solrServerUrl
	 * @throws MalformedURLException
	 * 
	 * @author rnojiri
	 */
	public CustomHttpSolrServer(String solrServerUrl, SolrServerMode indexingMode, int connectionTimeout, String name) throws MalformedURLException
	{
		super(solrServerUrl);
		
		this.name = name;

		if (indexingMode == null)
		{
			indexingMode = SolrServerMode.BINARY;
		}

		this.indexingMode = indexingMode;
		
		setConnectionTimeout(connectionTimeout);
		setAllowCompression(false);
		switch (indexingMode)
		{
		case BINARY:
			setRequestWriter(new BinaryRequestWriter());
			setParser(new BinaryResponseParser());
			return;
		case XML:
			setRequestWriter(new RequestWriter());
			setParser(new XMLResponseParser());
			return;
		}
	}

	/**
	 * @return the indexingMode
	 */
	public SolrServerMode getIndexingMode()
	{
		return indexingMode;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}
}
