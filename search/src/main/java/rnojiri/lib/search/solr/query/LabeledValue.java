package rnojiri.lib.search.solr.query;

/**
 * Faceted field label.
 * 
 * @author rnojiri
 */
public class LabeledValue
{
	public String label;
	public String value;

	/**
	 * @param label
	 * @param value
	 */
	public LabeledValue(String label, String value)
	{
		super();
		this.label = label;
		this.value = value;
	}
}