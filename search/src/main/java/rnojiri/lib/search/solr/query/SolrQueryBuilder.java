package rnojiri.lib.search.solr.query;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrQuery.SortClause;
import org.apache.solr.common.params.FacetParams;

import rnojiri.lib.support.Constants;

/**
 * Build solr queries.
 * 
 * @author rnojiri
 */
public class SolrQueryBuilder
{
	private static final String AND = "AND ";
	private static final String OR = "OR ";
	private static final String TO = " TO ";
	private static final String ALL = "*";
	private static final String FIELD_SEPARATOR = ":";
	private static final String RANGE_EQ_BEGIN = "[";
	private static final String RANGE_EQ_END = "]";
	private static final String RANGE_NEQ_BEGIN = "{";
	private static final String RANGE_NEQ_END = "}";
	private static final String OPEN_PARENTHESES = "(";
	private static final String CLOSE_PARENTHESES = ")";
	private static final String NEGATE = "-";
	private static final String MANDATORY_FIELD = "+";
	private static final String PROHIBITED_FIELD = "-";
	private static final String SELECT_ALL = ALL + FIELD_SEPARATOR + ALL;
	private static final String ALL_VALUES = RANGE_EQ_BEGIN + ALL + TO + ALL + RANGE_EQ_END;
	private static final String DOUBLE_BACKSLASH = "\\\\";
	private static final String QUAD_BACKSLASH = "\\\\\\\\";
	private static final String BACKSLASH = "\\";
	private static final String WHITESPACE = " ";
	private static final String EMPTY = "";
	private static final String FIELD_LABEL_OPEN = "{!key='";
	private static final String FIELD_LABEL_CLOSE = "'}";
	private static final String FUZZY = "~";
	private static final String BOOST = "^";
	private static final String ESCAPED_WHITESPACE = "\\\\ ";
	private static final String COUNT_ALL_FACETS_QUERY_PREFIX = "{!tag=dt}";
	private static final String COUNT_ALL_FACETS_FIELD_PREFIX = " !ex=dt";
	private static final String GROUP_BY = "group";
	private static final String GROUP_FIELD = GROUP_BY + ".field";
	private static final String GROUP_LIMIT = GROUP_BY + ".limit";
	private static final String STATS = "stats";
	private static final String STATS_FIELD = STATS + ".field";

	private static final String SOLR_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

	private StringBuilder stringBuilder;

	private SolrQuery solrQuery;

	private static final Pattern WHITESPACE_PATTERN = Pattern.compile(" ");
	private static final Pattern VARIABLE_PATTERN = Pattern.compile("^\\$\\{.*\\}");
	private static final Pattern RESERVED_CHARS = Pattern.compile("[\\/\\&\\|\\+\\-\\!\\(\\)\\{\\}\\[\\]\\^\\\"\\~\\?\\:\\\\]");

	private static final String METHOD_TO_STRING = "toString";

	private String label;

	private boolean ignoreQueueToCountFacets;

	/**
	 * Ranges from a variable inside the string.
	 * 
	 * @author rnojiri
	 */
	private class Range
	{
		public int beginning;
		public int end;
	}

	/**
	 * A variable.
	 * 
	 * @author rnojiri
	 */
	private class Variable
	{
		public String name;
		public SolrQueryBuilder.Range range;
	}

	private ArrayList<Variable> varRanges;

	/**
	 * Build a empty query using the default buffer size of 256 chars.
	 */
	public SolrQueryBuilder()
	{
		stringBuilder = new StringBuilder(Constants.BUFFER_SIZE_256);
		solrQuery = new SolrQuery();
		varRanges = new ArrayList<Variable>();
		ignoreQueueToCountFacets = false;
	}

	/**
	 * Build a empty query using the specified buffer size.
	 * 
	 * @param bufferSize
	 */
	public SolrQueryBuilder(int bufferSize)
	{
		stringBuilder = new StringBuilder(bufferSize);
		solrQuery = new SolrQuery();
		varRanges = new ArrayList<Variable>();
	}

	/**
	 * Copies another SolrQueryBuilder to this.
	 * 
	 * @param solrQueryBuilder
	 */
	public SolrQueryBuilder(SolrQueryBuilder solrQueryBuilder)
	{
		this.stringBuilder = new StringBuilder(solrQueryBuilder.stringBuilder.capacity());
		this.stringBuilder.append(solrQueryBuilder.stringBuilder);

		this.label = solrQueryBuilder.label;

		this.solrQuery = new SolrQuery();

		this.setupFacets(solrQueryBuilder.solrQuery.getFacetMinCount(), solrQueryBuilder.solrQuery.getFacetLimit(),
				(FacetParams.FACET_SORT_COUNT.equals(solrQueryBuilder.solrQuery.getFacetSortString())) ? true : false);

		String[] facetFields = solrQueryBuilder.solrQuery.getFacetFields();

		if (facetFields != null && facetFields.length > 0)
		{

			this.addFacetField(facetFields);
		}

		String[] facetQueries = solrQueryBuilder.solrQuery.getFacetQuery();

		if (facetQueries != null && facetQueries.length > 0)
		{

			for (String query : facetQueries)
			{
				this.solrQuery.addFacetQuery(query);
			}
		}

		String sortField = solrQueryBuilder.solrQuery.getSortField();

		if (sortField != null)
		{

			String[] array = WHITESPACE_PATTERN.split(sortField);

			this.addSortBy(array[0], (ORDER.asc.toString().equals(array[1]) ? true : false));
		}

		this.varRanges = new ArrayList<Variable>();
		this.varRanges.addAll(solrQueryBuilder.varRanges);
	}

	/**
	 * Returns all documents.
	 * 
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder all()
	{
		stringBuilder.append(SELECT_ALL);
		stringBuilder.append(WHITESPACE);

		return this;
	}

	/**
	 * Groups the results.
	 * 
	 * @param field
	 * @param limit
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder groupBy(String field, int limit)
	{
		solrQuery.add(GROUP_BY, Boolean.TRUE.toString());
		solrQuery.add(GROUP_FIELD, field);
		solrQuery.add(GROUP_LIMIT, String.valueOf(limit));
		
		return this;
	}

	/**
	 * Adds a field to the search.
	 * 
	 * @param field
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder field(String field)
	{
		stringBuilder.append(field);
		stringBuilder.append(FIELD_SEPARATOR);

		return this;
	}

	/**
	 * Adds a field to the search (can be negated).
	 * 
	 * @param field
	 * @param negate
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder field(String field, boolean negate)
	{
		if (negate)
		{
			stringBuilder.append(NEGATE);
		}

		stringBuilder.append(field);
		stringBuilder.append(FIELD_SEPARATOR);

		return this;
	}

	/**
	 * Sets a value as mandatory.
	 * 
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder mustHave()
	{
		stringBuilder.append(MANDATORY_FIELD);

		return this;
	}

	/**
	 * Sets a value as prohibited.
	 * 
	 * @return SolrQueryBuilder
	 * 
	 * @author rnojiri
	 */
	public SolrQueryBuilder mustNotHave()
	{
		stringBuilder.append(PROHIBITED_FIELD);

		return this;
	}

	/**
	 * Sets a Date value to the field.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder value(Date value)
	{
		if (value == null)
		{
			return this;
		}

		return nonVariableValue(getSolrDate(value), false, null, null);
	}

	/**
	 * Sets a SolrTime value to the field.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder value(SolrTime value)
	{
		if (value == null)
		{
			return this;
		}

		return nonVariableValue(value.getValue(), false, null, null);
	}

	/**
	 * Sets a boolean value to the field.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder value(boolean value)
	{
		return nonVariableValue(String.valueOf(value), false, null, null);
	}

	/**
	 * Sets a Number value to the field.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder value(Number value)
	{
		if (value == null)
		{
			return this;
		}

		return nonVariableValue(String.valueOf(value), false, null, null);
	}

	/**
	 * Sets multiple values to be searched in the field using OR operator.
	 * 
	 * @param clazz
	 *            <T>
	 * @param values
	 *            <T>
	 * @return SolrQueryBuilder
	 */
	public <T> SolrQueryBuilder withAnyValue(Class<T> clazz, Collection<T> values)
	{
		return withValues(true, clazz, values);
	}

	/**
	 * Sets multiple values to be searched in the field using AND operator.
	 * 
	 * @param clazz
	 *            <T>
	 * @param values
	 *            <T>
	 * @return SolrQueryBuilder
	 */
	public <T> SolrQueryBuilder withAllValues(Class<T> clazz, Collection<T> values)
	{
		return withValues(false, clazz, values);
	}

	/**
	 * Sets multiple values to be searched in the field using OR or AND operator.
	 * 
	 * @param any
	 * @param clazz
	 *            <T>
	 * @param values
	 *            <T>
	 * @return SolrQueryBuilder
	 */
	private <T> SolrQueryBuilder withValues(boolean any, Class<T> clazz, Collection<T> values)
	{
		try
		{
			Method toStringMethod = clazz.getMethod(METHOD_TO_STRING);

			if (values != null && values.size() > 0)
			{
				int size = values.size();

				if (size == 1)
				{

					Object object = values.iterator().next();

					if (object != null)
					{
						return value(object.toString());
					}

					return this;
				}

				openValueGroup();

				Iterator<T> iterator = values.iterator();

				while (iterator.hasNext())
				{
					T item = iterator.next();

					if (item != null)
					{
						value((String) toStringMethod.invoke(item));

						if (iterator.hasNext())
						{
							if (any) or();
							else and();
						}
					}
				}

				closeValueGroup();
			}

			return this;
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * Sets multiple values to be searched in the field using OR operator.
	 * 
	 * @param values
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder withAnyValue(String... values)
	{
		return withValues(true, String.class, values);
	}

	/**
	 * Sets multiple values to be searched in the field using AND operator.
	 * 
	 * @param values
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder withAllValues(String... values)
	{
		return withValues(false, String.class, values);
	}

	/**
	 * Sets multiple values to be searched in the field using OR operator.
	 * 
	 * @param values
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder withAnyValue(Number... values)
	{
		return withValues(true, Number.class, values);
	}

	/**
	 * Sets multiple values to be searched in the field using AND operator.
	 * 
	 * @param values
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder withAllValues(Number... values)
	{
		return withValues(false, Number.class, values);
	}

	/**
	 * Sets multiple values to be searched in the field using OR or AND operator.
	 * 
	 * @param any
	 * @param clazz
	 *            <T>
	 * @param values
	 *            <T>
	 * @return SolrQueryBuilder
	 */
	private <T> SolrQueryBuilder withValues(boolean any, Class<T> clazz, @SuppressWarnings("unchecked") T... values)
	{
		try
		{
			Method toStringMethod = clazz.getMethod(METHOD_TO_STRING);

			if (values != null && values.length > 0)
			{
				if (values.length == 1)
				{
					return value((String) toStringMethod.invoke(values[0]));
				}

				openValueGroup();

				for (int i = 0; i < values.length; i++)
				{
					value((String) toStringMethod.invoke(values[i]));

					if (i < values.length - 1)
					{
						if (any) or();
						else and();
					}
				}

				closeValueGroup();
			}

			return this;
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * Sets a value to the field.
	 * 
	 * @param value
	 */
	public SolrQueryBuilder value(String value)
	{
		return value(value, null, null);
	}

	/**
	 * Sets a value to the field using a fuzzy threshold.
	 * 
	 * @param value
	 * @param fuzzy
	 * @param boost
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder value(String value, float fuzzy)
	{
		return value(value, fuzzy, null);
	}

	/**
	 * Sets a value to the field using a fuzzy threshold.
	 * 
	 * @param value
	 * @param fuzzy
	 * @param boost
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder value(String value, int boost)
	{
		return value(value, null, boost);
	}

	/**
	 * Sets a value to the field using a fuzzy threshold.
	 * 
	 * @param value
	 * @param fuzzy
	 * @param boost
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder value(String value, Float fuzzy, Integer boost)
	{
		if (isVariable(value))
		{
			addVariable(value);
			stringBuilder.append(WHITESPACE);

			return this;
		}
		else
		{
			return nonVariableValue(value, true, fuzzy, boost);
		}
	}

	/**
	 * Sets a value to the field using a fuzzy threshold.
	 * 
	 * @param value
	 * @param scapeValue
	 * @param fuzzy
	 * @param boost
	 * @return SolrQueryBuilder
	 */
	private SolrQueryBuilder nonVariableValue(String value, boolean scapeValue, Float fuzzy, Integer boost)
	{
		String realValue = (scapeValue) ? escape(value) : value;

		stringBuilder.append(escapeWhiteSpaces(realValue));

		if (boost != null)
		{
			stringBuilder.append(BOOST);
			stringBuilder.append(boost);
		}
		else if (fuzzy != null)
		{
			stringBuilder.append(FUZZY);
			stringBuilder.append(fuzzy);
		}

		stringBuilder.append(WHITESPACE);

		return this;
	}

	/**
	 * Adds a new variable to the query.
	 * 
	 * @param name
	 */
	private void addVariable(String name)
	{
		Range range = new Range();

		range.beginning = stringBuilder.length();

		stringBuilder.append(name);

		range.end = stringBuilder.length();

		Variable variable = new Variable();

		variable.name = name.substring(2, name.length() - 1);

		variable.range = range;

		varRanges.add(variable);
	}

	/**
	 * Appends an AND to the query.
	 * 
	 * @return SolrQuery
	 */
	public SolrQueryBuilder and()
	{
		stringBuilder.append(AND);

		return this;
	}

	/**
	 * Appends an OR to the query.
	 * 
	 * @return SolrQuery
	 */
	public SolrQueryBuilder or()
	{
		stringBuilder.append(OR);

		return this;
	}

	/**
	 * Returns all document with value lesser or equal than the specified value.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allLesserOrEqualsThan(String value)
	{
		allBetween(ALL, value, true);

		return this;
	}

	/**
	 * Returns all document with date lesser or equal than the specified date.
	 * 
	 * @param date
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allLesserOrEqualsThan(Date date)
	{
		return allBetween(null, date, true);
	}

	/**
	 * Returns all document with date lesser or equal than the specified date.
	 * 
	 * @param date
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allLesserOrEqualsThan(SolrTime date)
	{
		return allBetween(null, date, true);
	}

	/**
	 * Returns all document with value lesser or equal than the specified value.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allLesserOrEqualsThan(Number value)
	{
		return allBetween(null, value, true);
	}

	/**
	 * Returns all document with value greater or equal than the specified value.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allGreaterOrEqualsThan(String value)
	{
		allBetween(value, ALL, true);

		return this;
	}

	/**
	 * Returns all document with date greater or equal than the specified date.
	 * 
	 * @param date
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allGreaterOrEqualsThan(Date date)
	{
		allBetween(date, null, true);

		return this;
	}

	/**
	 * Returns all document with date greater or equal than the specified date.
	 * 
	 * @param date
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allGreaterOrEqualsThan(SolrTime date)
	{
		allBetween(date, null, true);

		return this;
	}

	/**
	 * Returns all document with value greater or equal than the specified value.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allGreaterOrEqualsThan(Number value)
	{
		allBetween(value, null, true);

		return this;
	}

	/**
	 * Returns all document with value lesser than the specified value.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allLesserThan(String value)
	{
		allBetween(ALL, value, false);

		return this;
	}

	/**
	 * Returns all document with date lesser than the specified date.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allLesserThan(Date value)
	{
		allBetween(null, value, false);

		return this;
	}

	/**
	 * Returns all document with date lesser than the specified date.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allLesserThan(SolrTime value)
	{
		allBetween(null, value, false);

		return this;
	}

	/**
	 * Returns all document with value lesser than the specified value.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allLesserThan(Number value)
	{
		allBetween(null, value, false);

		return this;
	}

	/**
	 * Returns all document with value greater than the specified value.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allGreaterThan(String value)
	{
		allBetween(value, ALL, false);

		return this;
	}

	/**
	 * Returns all document with date greater than the specified date.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allGreaterThan(Date value)
	{
		allBetween(value, null, false);

		return this;
	}

	/**
	 * Returns all document with date greater than the specified date.
	 * 
	 * @param date
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allGreaterThan(SolrTime date)
	{
		allBetween(date, null, false);

		return this;
	}

	/**
	 * Returns all document with value greater than the specified value.
	 * 
	 * @param value
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allGreaterThan(Number value)
	{
		allBetween(value, null, false);

		return this;
	}

	/**
	 * Returns all document with date between than the specified range of dates (can include bounds if needed).
	 * 
	 * @param firstValue
	 * @param secondValue
	 * @param includeBounds
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allBetween(SolrTime firstValue, SolrTime secondValue, boolean includeBounds)
	{
		stringBuilder.append((includeBounds) ? RANGE_EQ_BEGIN : RANGE_NEQ_BEGIN);

		stringBuilder.append((firstValue != null) ? firstValue.getValue() : ALL);

		stringBuilder.append(TO);

		stringBuilder.append((secondValue != null) ? secondValue.getValue() : ALL);

		stringBuilder.append((includeBounds) ? RANGE_EQ_END : RANGE_NEQ_END);
		stringBuilder.append(WHITESPACE);

		return this;
	}

	/**
	 * Returns all document with value between than the specified range of values (can include bounds if needed).
	 * 
	 * @param firstValue
	 * @param secondValue
	 * @param includeBounds
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allBetween(String firstValue, String secondValue, boolean includeBounds)
	{
		stringBuilder.append((includeBounds) ? RANGE_EQ_BEGIN : RANGE_NEQ_BEGIN);

		if (isVariable(firstValue))
		{
			addVariable(firstValue);
		}
		else
		{
			stringBuilder.append(firstValue);
		}

		stringBuilder.append(TO);

		if (isVariable(secondValue))
		{
			addVariable(secondValue);
		}
		else
		{
			stringBuilder.append(secondValue);
		}

		stringBuilder.append((includeBounds) ? RANGE_EQ_END : RANGE_NEQ_END);
		stringBuilder.append(WHITESPACE);

		return this;
	}

	/**
	 * Returns all document with date between than the specified range of dates (can include bounds if needed).
	 * 
	 * @param firstValue
	 * @param secondValue
	 * @param includeBounds
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allBetween(Date firstValue, Date secondValue, boolean includeBounds)
	{
		stringBuilder.append((includeBounds) ? RANGE_EQ_BEGIN : RANGE_NEQ_BEGIN);

		stringBuilder.append((firstValue != null) ? getSolrDate(firstValue) : ALL);

		stringBuilder.append(TO);

		stringBuilder.append((secondValue != null) ? getSolrDate(secondValue) : ALL);

		stringBuilder.append((includeBounds) ? RANGE_EQ_END : RANGE_NEQ_END);
		stringBuilder.append(WHITESPACE);

		return this;
	}

	/**
	 * Returns all document with value between than the specified range of values (can include bounds if needed).
	 * 
	 * @param firstValue
	 * @param secondValue
	 * @param includeBounds
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder allBetween(Number firstValue, Number secondValue, boolean includeBounds)
	{
		stringBuilder.append((includeBounds) ? RANGE_EQ_BEGIN : RANGE_NEQ_BEGIN);

		stringBuilder.append((firstValue != null) ? firstValue.toString() : ALL);

		stringBuilder.append(TO);

		stringBuilder.append((secondValue != null) ? secondValue.toString() : ALL);

		stringBuilder.append((includeBounds) ? RANGE_EQ_END : RANGE_NEQ_END);
		stringBuilder.append(WHITESPACE);

		return this;
	}

	/**
	 * Sets a sort order.
	 * 
	 * @param field
	 * @param asc
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder addSortBy(String field, boolean asc)
	{
		solrQuery.addSort(new SortClause(field, (asc) ? ORDER.asc : ORDER.desc));

		return this;
	}

	/**
	 * Sets the pagination.
	 * 
	 * @param start
	 * @param rows
	 */
	public SolrQueryBuilder paginate(int start, int rows)
	{
		solrQuery.setStart(start);
		solrQuery.setRows(rows);

		return this;
	}

	/**
	 * Returns the native SolrQuery.
	 * 
	 * @return SolrQuery
	 */
	public SolrQuery getSolrQuery()
	{
		solrQuery.setQuery(this.toString());

		return solrQuery;
	}

	/**
	 * Returns the native solr query with all values setted.
	 * 
	 * @param values
	 * @return SolrQuery
	 */
	public SolrQuery getSolrQuery(Map<String, String> values)
	{
		if (varRanges.size() == 0)
		{
			return solrQuery;
		}

		solrQuery.setQuery(toString(values));

		return solrQuery;
	}

	/**
	 * Returns the native solr query with only one value.
	 * 
	 * @param values
	 * @return SolrQuery
	 */
	public SolrQuery getSolrQuery(String key, String value)
	{
		if (varRanges.size() == 0)
		{
			return solrQuery;
		}

		Map<String, String> valueMap = new HashMap<String, String>();
		valueMap.put(key, value);

		solrQuery.setQuery(toString(valueMap));

		return solrQuery;
	}

	/**
	 * Returns the String solr query with all values setted.
	 * 
	 * @param values
	 * @return String
	 */
	public String toString(Map<String, String> values)
	{
		if (varRanges.size() == 0)
		{
			return toString();
		}

		StringBuilder copy = new StringBuilder(stringBuilder.capacity());

		copy.append(stringBuilder);

		for (int i = varRanges.size() - 1; i >= 0; i--)
		{

			Variable variable = varRanges.get(i);

			Range range = variable.range;

			if (range == null)
			{
				continue;
			}

			String value = values.get(variable.name);

			if (value == null)
			{
				continue;
			}

			value = escapeWhiteSpaces(value);

			copy.replace(range.beginning, range.end, value);
		}

		return getCroppedQuery(copy);
	}

	@Override
	public String toString()
	{
		return getCroppedQuery(stringBuilder);
	}

	/**
	 * Crops the query.
	 * 
	 * @param buffer
	 * @return String
	 */
	private String getCroppedQuery(StringBuilder buffer)
	{
		int size = buffer.length();

		if (size > 0)
		{
			return buffer.substring(0, buffer.length() - 1).toString();
		}
		else
		{
			return EMPTY;
		}
	}

	/**
	 * Check for white spaces and replaces them for the escaped version.
	 * 
	 * @param value
	 * @return String
	 */
	private String escapeWhiteSpaces(String value)
	{
		return WHITESPACE_PATTERN.matcher(value).replaceAll(ESCAPED_WHITESPACE);
	}

	/**
	 * Check for a variable.
	 * 
	 * @param value
	 * @return boolean
	 */
	private boolean isVariable(String value)
	{
		return VARIABLE_PATTERN.matcher(value).find();
	}

	/**
	 * Adds a field existence check in the query.
	 * 
	 * @param name
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder fieldExists(String name)
	{
		stringBuilder.append(name);
		stringBuilder.append(FIELD_SEPARATOR);
		stringBuilder.append(ALL_VALUES);
		stringBuilder.append(WHITESPACE);

		return this;
	}

	/**
	 * Adds the field not existence check in the query.
	 * 
	 * @param name
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder fieldNotExists(String name)
	{
		stringBuilder.append(NEGATE);

		return fieldExists(name);
	}

	/**
	 * Escapes all reserved characters.
	 * 
	 * @param value
	 * @return String
	 */
	private String escape(String value)
	{
		Matcher matcher = RESERVED_CHARS.matcher(value);

		if (!matcher.find())
		{
			return value;
		}
		else
		{
			StringBuffer escapedValueBuffer = new StringBuffer(Constants.BUFFER_SIZE_4096);

			do
			{
				String group = matcher.group();

				matcher.appendReplacement(escapedValueBuffer, (!group.equals(BACKSLASH)) ? DOUBLE_BACKSLASH + group : QUAD_BACKSLASH);
			}
			while (matcher.find());

			matcher.appendTail(escapedValueBuffer);

			return escapedValueBuffer.toString();
		}
	}

	/**
	 * Opens a new search group.
	 * 
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder openValueGroup()
	{
		stringBuilder.append(OPEN_PARENTHESES);

		return this;
	}

	/**
	 * closes the search group.
	 * 
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder closeValueGroup()
	{
		stringBuilder.deleteCharAt(stringBuilder.length() - 1);
		stringBuilder.append(CLOSE_PARENTHESES);
		stringBuilder.append(WHITESPACE);

		return this;
	}

	/**
	 * Enables the faceted results.
	 * 
	 * @param minCount
	 * @param limit
	 * @param orderByCount
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder setupFacets(int minCount, int limit, boolean orderByCount)
	{
		solrQuery.setFacetMinCount(minCount);
		solrQuery.setFacetLimit(limit);
		solrQuery.setFacetSort((orderByCount) ? FacetParams.FACET_SORT_COUNT : FacetParams.FACET_SORT_INDEX);

		return this;
	}

	/**
	 * Adds the faceted fields.
	 * 
	 * @param fields
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder addFacetField(String... fields)
	{
		if (fields != null && fields.length > 0)
		{
			solrQuery.addFacetField(fields);
		}

		return this;
	}

	/**
	 * Adds a faceted field and its label.
	 * 
	 * @param label
	 * @param field
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder addLabeledFacetField(String label, String field)
	{
		solrQuery.addFacetField(addFieldProperties(label, field));

		return this;
	}

	/**
	 * Adds various faceted field and its labels.
	 * 
	 * @param fields
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder addFacetField(LabeledValue... fields)
	{
		if (fields != null && fields.length > 0)
		{
			for (LabeledValue field : fields)
			{
				solrQuery.addFacetField(addFieldProperties(field.label, field.value));
			}
		}

		return this;
	}

	/**
	 * Returns the label from a faceted field.
	 * 
	 * @param label
	 * @param value
	 * @return String
	 */
	private String addFieldProperties(String label, String value)
	{
		StringBuilder builder = new StringBuilder(Constants.BUFFER_SIZE_64);
		builder.append(FIELD_LABEL_OPEN);
		builder.append(label);
		if (ignoreQueueToCountFacets) builder.append(COUNT_ALL_FACETS_FIELD_PREFIX);
		builder.append(FIELD_LABEL_CLOSE);
		builder.append(value);

		return builder.toString();
	}

	/**
	 * Returns the raw query.
	 * 
	 * @return String
	 */
	public String getRawQuery()
	{
		solrQuery.setQuery(getCroppedQuery(stringBuilder));

		return solrQuery.toString();
	}

	/**
	 * Converts a data to solr format.
	 * 
	 * @return String
	 */
	private String getSolrDate(Date date)
	{
		return (new SimpleDateFormat(SOLR_DATE_FORMAT)).format(convertToZuluDate(date));
	}

	/**
	 * Converts the date to zulu date.
	 * 
	 * @param date
	 * @return Date
	 */
	private Date convertToZuluDate(Date date)
	{
		TimeZone timeZone = TimeZone.getDefault();

		Date value = new Date(date.getTime() - timeZone.getRawOffset());

		if (timeZone.inDaylightTime(value))
		{
			Date dstDate = new Date(value.getTime() - timeZone.getDSTSavings());

			if (timeZone.inDaylightTime(dstDate))
			{
				value = dstDate;
			}
		}

		return value;
	}

	/**
	 * Adds a facet query.
	 * 
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder addFacetQuery(SolrQueryBuilder facetQuery)
	{
		solrQuery.addFacetQuery(addFieldProperties(facetQuery.label, facetQuery.toString()));

		return this;
	}

	/**
	 * Adds various facet queries.
	 * 
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder addFacetQuery(SolrQueryBuilder... facetQuery)
	{
		if (facetQuery != null && facetQuery.length > 0)
		{
			for (SolrQueryBuilder item : facetQuery)
			{
				addFacetQuery(item);
			}
		}

		return this;
	}

	/**
	 * @return the label
	 */
	public String getLabel()
	{
		return label;
	}

	/**
	 * @param label
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder setLabel(String label)
	{
		this.label = label;

		return this;
	}

	/**
	 * Appends another query.
	 * 
	 * @param other
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder append(SolrQueryBuilder other)
	{
		stringBuilder.append(other.toString());
		stringBuilder.append(WHITESPACE);

		return this;
	}

	/**
	 * Use a specific html tag to highlight.
	 * 
	 * @param htmlTag
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder highlightHtmlTag(String htmlTag)
	{
		solrQuery.setHighlightSimplePre(new StringBuilder(Constants.BUFFER_SIZE_16).append('<').append(htmlTag).append('>').toString());
		solrQuery.setHighlightSimplePost(new StringBuilder(Constants.BUFFER_SIZE_16).append('<').append('/').append(htmlTag).append('>').toString());

		return this;
	}

	/**
	 * Enable highlight for specific fields.
	 * 
	 * @param fields
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder highlightField(String... fields)
	{
		if (fields != null && fields.length > 0)
		{
			solrQuery.setHighlight(true);

			for (String field : fields)
			{
				solrQuery.addHighlightField(field);
			}
		}

		return this;
	}

	/**
	 * Returns the length.
	 * 
	 * @return int
	 */
	public int getLength()
	{
		return stringBuilder.length();
	}

	/**
	 * Ignores the main query to count the facets.
	 * 
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder ignoreQueryToCountFacets()
	{
		stringBuilder.insert(0, COUNT_ALL_FACETS_QUERY_PREFIX);

		ignoreQueueToCountFacets = true;

		return this;
	}

	/**
	 * Adds the field statistics.
	 * 
	 * @param field
	 * @return SolrQueryBuilder
	 */
	public SolrQueryBuilder stats(String ... field)
	{
		if(field != null && field.length > 0)
		{
			solrQuery.add(STATS, Boolean.TRUE.toString());
			
			for(String item : field)
			{
				solrQuery.add(STATS_FIELD, item);
			}
		}
		
		return this;
	}
}
