package rnojiri.lib.search.solr.service;

/**
 * An abstract search parameters holder class.
 * 
 * @author rnojiri
 */
public abstract class AbstractSearchParameters
{
	public final static String SORT_MODE_ASC = "asc";
	public final static String SORT_MODE_DESC = "desc";
	
	protected String sortBy;
	
	protected String sortMode;

	/**
	 * @return the sortBy
	 */
	public String getSortBy()
	{
		return sortBy;
	}

	/**
	 * @param sortBy the sortBy to set
	 */
	public void setSortBy(String sortBy)
	{
		this.sortBy = sortBy;
	}

	/**
	 * @return the sortMode
	 */
	public String getSortMode()
	{
		return sortMode;
	}

	/**
	 * @param sortMode the sortMode to set
	 */
	public void setSortMode(String sortMode)
	{
		this.sortMode = sortMode;
	}
}
