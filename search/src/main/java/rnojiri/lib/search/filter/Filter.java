package rnojiri.lib.search.filter;

import java.io.Serializable;
import java.util.List;

/**
 * Search result filter informations.
 * 
 * @author rnojiri
 */
public class Filter implements Comparable<Filter>, Serializable
{
	private static final long serialVersionUID = -4404280751970923192L;

	private final String filterName;
	
	private final String filterParam;
	
	private List<FilterProperty> filterProperties;
	
	private int order;

	/**
	 * @param filterName
	 * @param filterParam
	 * @param filterProperties
	 * @param order
	 */
	public Filter(String filterName, String filterParam, List<FilterProperty> filterProperties, int order)
	{
		this.filterName = filterName;
		this.filterParam = filterParam;
		this.filterProperties = filterProperties;
		this.order = order;
	}
	
	/**
	 * @param filterName
	 * @param filterParam
	 * @param filterProperties
	 */
	public Filter(String filterName, String filterParam, List<FilterProperty> filterProperties)
	{
		this(filterName, filterParam, filterProperties, 0);
	}

	/**
	 * @return the filterName
	 */
	public String getFilterName()
	{
		return filterName;
	}

	/**
	 * @return the filterParam
	 */
	public String getFilterParam()
	{
		return filterParam;
	}

	/**
	 * @return the filterProperties
	 */
	public List<FilterProperty> getFilterProperties()
	{
		return filterProperties;
	}
	
	/**
	 * @param filterProperties the filterProperties to set
	 */
	public void setFilterProperties(List<FilterProperty> filterProperties)
	{
		this.filterProperties = filterProperties;
	}

	@Override
	public int compareTo(Filter o)
	{
		if(this.order == o.order) return 0;
		if(this.order > o.order) return 1; else return -1;
	}
}
